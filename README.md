# NACSOS Pipelines

## Usage

Install the packages (these commands will install both `nacsos_lib` and `nacsos_pipes` at once):

```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
pip install -r requirements_dev.txt
```

When you are developing things, it might be useful to `pip install -e ../nacsos_data`.
Keep in mind to update the git reference in `nacsos_lib/requirements.txt` though!

Configuration for running in dev-mode:
![](img/pycharm.png)

Known issue: In some cases when a job fails hard and the `--reload` option is set, 
it leaves orphaned processes that block the uvicorn port. They are not straight forward
to kill, so you can use the following:
```bash
# check for orphaned processes
% ps -fA | grep nacsos_pipes
# using a process id found with the command above, kill it
sudo kill -s SIGKILL <pid>
# ... or automatically kill all orphans
% ps -fA | grep nacsos_pipes | cut -d " " -f8 | xargs -n1 sudo kill -s SIGKILL 
```