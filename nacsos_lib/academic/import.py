import uuid
import logging
import pathlib
from typing import TYPE_CHECKING, Callable, Generator

from nacsos_data.util.academic.importer import import_academic_items
from nacsos_data.util.academic.openalex import generate_items_from_openalex, translate_doc, translate_work
from nacsos_data.db import DatabaseEngineAsync
from nacsos_data.models.items.academic import AcademicItemModel
from nacsos_data.models.pipeline import CPULoadClassification
from nacsos_data.models.openalex.solr import OpType, SearchField, DefType, WorkSolr

from nacsos_pipes.common import library, settings, get_db_engine_async
from nacsos_pipes.common.disk.serializers.scopus import ScopusCSVSerializer
from nacsos_pipes.common.task.artefacts import Artefact
from nacsos_pipes.common.disk.serializers import WebOfScienceSerializer, JSONLSerializer
from nacsos_pipes.executor.types import TaskContext

if TYPE_CHECKING:
    from sqlalchemy.ext.asyncio import AsyncSession  # noqa: F401


@library.func(
    name='Web of Science file import',
    artefacts={},
    est_cpu_load=CPULoadClassification.LOW,
    # we may only keep this for the log, but it could be deleted right away
    recommended_lifetime=7,
    runtime_estimator=lambda n: n * 0.1,
    memory_estimator=lambda n: 200,
    tags=['academic'])
async def import_wos_file(records: Artefact[WebOfScienceSerializer[AcademicItemModel]],
                          project_id: str | None = None,
                          import_id: str | None = None,
                          ctx: TaskContext | None = None) -> None:
    """
    Import Web of Science files in ISI format.
    Each record will be checked for duplicates within the project.

    `project_id` and `import_id` can be set to automatically populate the many-to-many tables
    and link the data to an import or project.

    **records**
        An Artefact with WoS isi filenames.
    **project_id**
        The project_id to connect these items to (required)
    **import_id**
        The import_id to connect these items to (required)
    """
    if not records.filenames or len(records.filenames) == 0:
        raise ValueError('Records artefact does appear to miss files!')
    if import_id is None:
        raise ValueError('Import ID is not set!')
    if project_id is None:
        raise ValueError('Project ID is not set!')

    if ctx is None:
        ctx = TaskContext(logger=logging.getLogger('import_wos_file'),
                          target_directory=pathlib.Path.cwd(),
                          working_directory=pathlib.Path.cwd())

    def _read_file(filename: str) -> Callable[[], Generator[AcademicItemModel, None, None]]:
        def inner() -> Generator[AcademicItemModel, None, None]:
            for itm in WebOfScienceSerializer[AcademicItemModel](filepath=filename).read():
                itm.item_id = uuid.uuid4()
                yield itm

        return inner

    db_engine = DatabaseEngineAsync(host=settings.DB_CORE.HOST, port=settings.DB_CORE.PORT,
                                    user=settings.DB_CORE.USER, password=settings.DB_CORE.PASSWORD,
                                    database=settings.DB_CORE.DATABASE)

    for file in records.filenames:
        ctx.logger.info(f'Importing articles from web of science file: {file}')
        async with db_engine.session() as session:
            await import_academic_items(
                session=session,
                project_id=project_id,
                new_items=_read_file(file),
                import_name=None,
                description=None,
                user_id=None,
                import_id=import_id,
                vectoriser=None,
                max_slop=0.05,
                batch_size=5000,
                dry_run=False,
                trust_new_authors=False,
                trust_new_keywords=False,
                log=ctx.logger
            )


@library.func(
    name='Scopus file import',
    artefacts={},
    est_cpu_load=CPULoadClassification.LOW,
    # we may only keep this for the log, but it could be deleted right away
    recommended_lifetime=7,
    runtime_estimator=lambda n: n * 0.1,
    memory_estimator=lambda n: 200,
    tags=['academic'])
async def import_scopus_csv_file(records: Artefact[ScopusCSVSerializer[AcademicItemModel]],
                                 project_id: str | None = None,
                                 import_id: str | None = None,
                                 ctx: TaskContext | None = None) -> None:
    """
    Import Scopus files in CSV format.
    Consult the [documentation](https://apsis.mcc-berlin.net/nacsos-docs/user/import/) before continuing!
    Each record will be checked for duplicates within the project.

    `project_id` and `import_id` can be set to automatically populate the many-to-many tables
    and link the data to an import or project.

    **records**
        An Artefact with scopus csv filenames.
    **project_id**
        The project_id to connect these items to (required)
    **import_id**
        The import_id to connect these items to (required)
    """
    if not records.filenames or len(records.filenames) == 0:
        raise ValueError('Records artefact does appear to miss files!')
    if import_id is None:
        raise ValueError('Import ID is not set!')
    if project_id is None:
        raise ValueError('Project ID is not set!')

    if ctx is None:
        ctx = TaskContext(logger=logging.getLogger('import_scopus_file'),
                          target_directory=pathlib.Path.cwd(),
                          working_directory=pathlib.Path.cwd())

    def _read_file(filename: str) -> Callable[[], Generator[AcademicItemModel, None, None]]:
        def inner() -> Generator[AcademicItemModel, None, None]:
            for itm in ScopusCSVSerializer[AcademicItemModel](filepath=filename).read():
                itm.item_id = uuid.uuid4()
                yield itm

        return inner

    db_engine = DatabaseEngineAsync(host=settings.DB_CORE.HOST, port=settings.DB_CORE.PORT,
                                    user=settings.DB_CORE.USER, password=settings.DB_CORE.PASSWORD,
                                    database=settings.DB_CORE.DATABASE)

    for file in records.filenames:
        ctx.logger.info(f'Importing articles from scopus CSV file: {file}')
        async with db_engine.session() as session:
            await import_academic_items(
                session=session,
                project_id=project_id,
                new_items=_read_file(file),
                import_name=None,
                description=None,
                user_id=None,
                import_id=import_id,
                vectoriser=None,
                max_slop=0.05,
                batch_size=5000,
                dry_run=False,
                trust_new_authors=False,
                trust_new_keywords=False,
                log=ctx.logger
            )


@library.func(
    name='Import from OpenAlex-Solr',
    artefacts={},
    est_cpu_load=CPULoadClassification.LOW,
    # we may only keep this for the log, but it could be deleted right away
    recommended_lifetime=7,
    runtime_estimator=lambda n: n * 0.1,
    memory_estimator=lambda n: 200,
    tags=['academic'])
async def import_openalex(query: str,
                          def_type: DefType = 'lucene',
                          field: SearchField = 'title_abstract',
                          op: OpType = 'AND',
                          project_id: str | None = None,
                          import_id: str | None = None,
                          ctx: TaskContext | None = None) -> None:
    """
    Import items from our self-hosted Solr database.
    Each record will be checked for duplicates within the project.

    `project_id` and `import_id` can be set to automatically populate the many-to-many tables
    and link the data to an import or project.

    **query**
        The solr query to run
    **def_type**
        The solr parser to use, typically this is 'lucene'
    **field**
        The field the search is executed on, often this is set to title & abstract
    **op**
        Usually you want this to be set to 'AND'
    **project_id**
        The project_id to connect these items to (required)
    **import_id**
        The import_id to connect these items to (required)
    """
    if import_id is None:
        raise ValueError('Import ID is not set!')
    if project_id is None:
        raise ValueError('Project ID is not set!')

    if ctx is None:
        ctx = TaskContext(logger=logging.getLogger('import_openalex'),
                          target_directory=pathlib.Path.cwd(),
                          working_directory=pathlib.Path.cwd())

    db_engine = DatabaseEngineAsync(host=settings.DB_CORE.HOST, port=settings.DB_CORE.PORT,
                                    user=settings.DB_CORE.USER, password=settings.DB_CORE.PASSWORD,
                                    database=settings.DB_CORE.DATABASE)

    def _read_openalex() -> Generator[AcademicItemModel, None, None]:
        for itm in generate_items_from_openalex(
                query=query,
                openalex_endpoint=settings.OPENALEX_URL,
                def_type=def_type,
                field=field,
                op=op,
                batch_size=1000,
                log=ctx.logger
        ):
            itm.item_id = uuid.uuid4()
            yield itm

    async with db_engine.session() as session:
        await import_academic_items(
            session=session,
            project_id=project_id,
            new_items=_read_openalex,
            import_name=None,
            description=None,
            user_id=None,
            import_id=import_id,
            vectoriser=None,
            max_slop=0.05,
            batch_size=5000,
            dry_run=False,
            trust_new_authors=False,
            trust_new_keywords=False,
            log=ctx.logger
        )


@library.func(
    name='AcademicItem import',
    artefacts={},
    est_cpu_load=CPULoadClassification.LOW,
    # we may only keep this for the log, but it could be deleted right away
    recommended_lifetime=7,
    runtime_estimator=lambda n: n * 0.1,
    memory_estimator=lambda n: 200,
    tags=['academic'])
async def import_academic_db(articles: Artefact[JSONLSerializer[AcademicItemModel]],
                             project_id: str | None = None,
                             import_id: str | None = None,
                             ctx: TaskContext | None = None) -> None:
    """
    Import articles that are in the exact format of how AcademicItems are stored in the database.
    We assume one JSON-encoded AcademicItemModel per line.

    `project_id` and `import_id` can be set to automatically populate the M2M tables
    and link the data to an import or project.

    **articles**
        An Artefact of a AcademicItems
    **project_id**
        The project_id to connect these tweets to
    **import_id**
        The import_id to connect these tweets to
    """
    db_engine = get_db_engine_async()

    if ctx is None:
        ctx = TaskContext(logger=logging.getLogger('import_academic_file'),
                          target_directory=pathlib.Path.cwd(),
                          working_directory=pathlib.Path.cwd())

    if articles.filenames is not None:
        files = articles.filenames
    elif articles.filename is not None:
        files = [articles.filename]
    else:
        raise AttributeError('No source file(s) specified!')

    if import_id is None:
        raise ValueError('Import ID is not set!')
    if project_id is None:
        raise ValueError('Project ID is not set!')

    def _read_file(filename: str) -> Callable[[], Generator[AcademicItemModel, None, None]]:
        def inner() -> Generator[AcademicItemModel, None, None]:
            with JSONLSerializer[dict](filepath=filename) as serializer:  # type: ignore[type-arg]
                for article in serializer.read():
                    itm = AcademicItemModel.model_validate(article)
                    itm.item_id = uuid.uuid4()
                    yield itm

        return inner

    for file in files:
        ctx.logger.info(f'Importing articles (AcademicItemModel-formatted) from file: {file}')
        async with db_engine.session() as session:
            await import_academic_items(
                session=session,
                project_id=project_id,
                new_items=_read_file(file),
                import_name=None,
                description=None,
                user_id=None,
                import_id=import_id,
                vectoriser=None,
                max_slop=0.05,
                batch_size=5000,
                dry_run=False,
                trust_new_authors=False,
                trust_new_keywords=False,
                log=ctx.logger
            )


@library.func(
    name='OpenAlex file import',
    artefacts={},
    est_cpu_load=CPULoadClassification.LOW,
    # we may only keep this for the log, but it could be deleted right away
    recommended_lifetime=7,
    runtime_estimator=lambda n: n * 0.1,
    memory_estimator=lambda n: 200,
    tags=['academic'])
async def import_openalex_file(articles: Artefact[JSONLSerializer[AcademicItemModel]],
                               project_id: str | None = None,
                               import_id: str | None = None,
                               ctx: TaskContext | None = None) -> None:
    """
    Import articles that are in the OpenAlex format used in our solr database.
    We assume one JSON-encoded WorkSolr object per line.

    `project_id` and `import_id` can be set to automatically populate the M2M tables
    and link the data to an import or project.

    **articles**
        An Artefact of a solr export
    **project_id**
        The project_id to connect these tweets to
    **import_id**
        The import_id to connect these tweets to
    """
    db_engine = get_db_engine_async()

    if ctx is None:
        ctx = TaskContext(logger=logging.getLogger('import_openalex_file'),
                          target_directory=pathlib.Path.cwd(),
                          working_directory=pathlib.Path.cwd())

    if articles.filenames is not None:
        files = articles.filenames
    elif articles.filename is not None:
        files = [articles.filename]
    else:
        raise AttributeError('No source file(s) specified!')
    if import_id is None:
        raise ValueError('Import ID is not set!')
    if project_id is None:
        raise ValueError('Project ID is not set!')

    def _read_file(filename: str) -> Callable[[], Generator[AcademicItemModel, None, None]]:
        def inner() -> Generator[AcademicItemModel, None, None]:
            with JSONLSerializer[dict](filepath=filename) as serializer:  # type: ignore[type-arg]
                for article in serializer.read():
                    itm = translate_work(translate_doc(WorkSolr.model_validate(article)))
                    itm.item_id = uuid.uuid4()
                    yield itm

        return inner

    for file in files:
        ctx.logger.info(f'Importing articles (WorkSolr-formatted) from file: {file}')
        async with db_engine.session() as session:
            await import_academic_items(
                session=session,
                project_id=project_id,
                new_items=_read_file(file),
                import_name=None,
                description=None,
                user_id=None,
                import_id=import_id,
                vectoriser=None,
                max_slop=0.05,
                batch_size=5000,
                dry_run=False,
                trust_new_authors=False,
                trust_new_keywords=False,
                log=ctx.logger
            )
