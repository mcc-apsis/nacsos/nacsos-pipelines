import json
from typing import Literal, Any
from searchtweets import ResultStream

from nacsos_pipes.common import library
from nacsos_pipes.common.task.artefacts import Artefact
from nacsos_pipes.executor.types import TaskContext
from nacsos_pipes.common.disk.serializers import JSONLSerializer


@library.func(
    name='Twitter Search API',
    tags=['twitter'],
    artefacts={
        'tweets': Artefact[Any](filename='raw_tweets.jsonl',
                                serializer=JSONLSerializer,  # type: ignore[arg-type]
                                dtype=dict)  # type: ignore[arg-type]
    })
def search_twitter(query: str,
                   bearer_token: str,
                   next_token: str | None = None,
                   since_id: str | None = None,
                   until_id: str | None = None,
                   sort_order: Literal['recency', 'relevancy'] = 'recency',
                   start_time: str | None = None,
                   end_time: str | None = None,
                   results_per_response: int = 500,
                   max_requests: int = -1,
                   max_tweets: int = -1,
                   ctx: TaskContext | None = None) -> None:
    """
    Docs: https://developer.twitter.com/en/docs/twitter-api/tweets/search/introduction
    Query builder: https://developer.twitter.com/apitools/query?query=

    WARNING:
    Your credentials will be stored in the database in plaintext!
    Do not assume this to be a secret!

    API limits:
        - 1 request/second
        - 300 requests/15min
        - 500 results per response
        - 1024 characters per query

    **bearer_token**
        Your bearer token that you can get from https://developer.twitter.com/
    **max_requests**
        The maximum number of requests to make.
        This is meant as a failsafe, set to -1 (unlimited) if you are feeling lucky.
    **max_tweets**
        The total maximum number of tweets to retrieve.
        This is meant as a failsafe, set to -1 (unlimited) if you are feeling lucky.
    **results_per_response/max_results**
        The maximum number of search results to be returned by a request. A number between 10 and the system limit
        (currently 500). By default, a request response will return 10 results.
    **next_token**
        This parameter is used to get the next 'page' of results. The value used with the parameter is
        pulled directly from the response provided by the API, and should not be modified. You can learn more
        by visiting our page on pagination.
    **since_id**
        Returns results with a Tweet ID greater than (for example, more recent than) the specified ID.
        The ID specified is exclusive and responses will not include it. If included with the same request as
        a start_time parameter, only since_id will be used.
    **until_id**
        Returns results with a Tweet ID less than (that is, older than) the specified ID. Used with since_id.
        The ID specified is exclusive and responses will not include it.
    **sort_order**
        This parameter is used to specify the order in which you want the Tweets returned.
        By default, a request will return the most recent Tweets first (sorted by recency).
    **start_time**
        YYYY-MM-DDTHH:mm:ssZ (ISO 8601/RFC 3339). The oldest UTC timestamp from which the Tweets will be provided.
        Timestamp is in second granularity and is inclusive (for example, 12:00:01 includes the first second of the
        minute). By default, a request will return Tweets from up to 30 days ago if you do not include this parameter.
    **end_time**
        YYYY-MM-DDTHH:mm:ssZ (ISO 8601/RFC 3339). Used with start_time. The newest, most recent UTC timestamp
        to which the Tweets will be provided. Timestamp is in second granularity and is exclusive
        (for example, 12:00:01 excludes the first second of the minute). If used without start_time,
        Tweets from 30 days before end_time will be returned by default. If not specified, end_time will
        default to [now - 30 seconds].
    """
    if ctx is None:
        raise RuntimeError('Missing context')
    ctx.logger.info('')

    request_params = {
        'query': query,
        'tweet.fields': 'attachments,author_id,conversation_id,created_at,entities,geo,id,'
                        'in_reply_to_user_id,lang,possibly_sensitive,public_metrics,referenced_tweets,reply_settings,'
                        'source,text,withheld,context_annotations',
        'expansions': 'attachments.media_keys,attachments.poll_ids,author_id,entities.mentions.username,'
                      'geo.place_id,in_reply_to_user_id',
        # additional expansions: referenced_tweets.id,referenced_tweets.id.author_id
        'media.fields': 'alt_text,duration_ms,height,media_key,preview_image_url,public_metrics,type,url,variants,width',
        'poll.fields': 'duration_minutes,end_datetime,id,options,voting_status',
        'user.fields': 'created_at,description,entities,id,location,name,pinned_tweet_id,profile_image_url,'
                       'protected,public_metrics,url,username,verified,withheld',
        'place.fields': 'contained_within,country,country_code,full_name,geo,id,name,place_type'
    }
    if next_token:
        request_params['next_token'] = next_token
    if since_id:
        request_params['since_id'] = since_id
    if until_id:
        request_params['until_id'] = until_id
    if sort_order:
        request_params['sort_order'] = sort_order
    if start_time:
        request_params['start_time'] = start_time
    if end_time:
        request_params['end_time'] = end_time
    if results_per_response:
        request_params['max_results'] = results_per_response  # type: ignore[assignment]

    if max_requests < 0:
        max_requests = None  # type: ignore[assignment]
    if max_tweets < 0:
        max_tweets = None  # type: ignore[assignment]

    stream = ResultStream(
        endpoint='https://api.twitter.com/2/tweets/search/all',
        request_parameters=request_params,
        bearer_token=bearer_token,
        max_tweets=max_tweets,
        max_requests=max_requests,
        output_format='r')

    with open(ctx.target_directory / 'raw_tweets.jsonl', 'w') as f_out:
        for results in stream.stream():
            if 'data' in results and type(results['data']) is list:
                ctx.logger.info(f'Received page with {len(results["data"])} tweets!')
                f_out.write(json.dumps(results) + '\n')
            else:
                ctx.logger.error('Something went wrong!')
