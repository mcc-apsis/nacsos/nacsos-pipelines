import logging
import pathlib
from datetime import datetime
from typing import Generator, Any

from nacsos_data.models.pipeline import CPULoadClassification
from nacsos_data.db.crud.items.twitter import import_tweet, import_tweets
from nacsos_data.models.items.twitter import \
    TwitterItemModel, \
    ReferencedTweet, \
    Hashtag, \
    Cashtag, \
    URL, \
    Mention, \
    TwitterUserModel, \
    ContextAnnotation

from nacsos_pipes.common import library, get_db_engine_async
from nacsos_pipes.common.task.artefacts import Artefact
from nacsos_pipes.executor.types import TaskContext
from nacsos_pipes.common.disk.serializers import JSONLSerializer


def _api_page_to_tweets(page: dict[str, Any]) -> Generator[TwitterItemModel, None, None]:
    """
    This function takes the dictionary (JSON response) from the Twitter search API
    and yields consolidated `TwitterItemModel`s.
    """
    users = {}
    if 'includes' in page and 'users' in page['includes']:
        users = {user['id']: user for user in page['includes']['users']}

    for tweet in page['data']:
        user = None
        if tweet['author_id'] in users:
            user_obj = users[tweet['author_id']]
            user = TwitterUserModel(
                id=tweet['author_id'],
                created_at=datetime.strptime(user_obj['created_at'][:19], '%Y-%m-%dT%H:%M:%S'),
                name=None if user_obj['name'] == user_obj['username'] else user_obj['name'],
                username=user_obj['username'],
                verified=user_obj['verified'],
                description=user_obj.get('description'),
                location=user_obj.get('location'),
                followers_count=user_obj.get('public_metrics', {}).get('followers_count'),
                following_count=user_obj.get('public_metrics', {}).get('following_count'),
                tweet_count=user_obj.get('public_metrics', {}).get('tweet_count'),
                listed_count=user_obj.get('public_metrics', {}).get('listed_count')
            ).dict(exclude_none=True)

        ref_tweets = None
        if 'referenced_tweets' in tweet:
            ref_tweets = [ReferencedTweet(id=ref_tweet['id'], type=ref_tweet['type'])
                          for ref_tweet in tweet['referenced_tweets']]
        latlon = None
        if 'geo' in tweet and 'coordinates' in tweet['geo'] and tweet['geo'].get('type') == 'Point':
            latlon = tweet['geo']['coordinates'].get('coordinates')

        hashtags = None
        if 'entities' in tweet and 'hashtags' in tweet['entities']:
            hashtags = [Hashtag(start=ht['start'], end=ht['end'], tag=ht['tag'])
                        for ht in tweet['entities']['hashtags']]
        cashtags = None
        if 'entities' in tweet and 'cashtags' in tweet['entities']:
            cashtags = [Cashtag(start=ct['start'], end=ct['end'], tag=ct['tag'])
                        for ct in tweet['entities']['cashtags']]
        urls = None
        if 'entities' in tweet and 'urls' in tweet['entities']:
            urls = [URL(start=url['start'], end=url['end'], url=url['url'], url_expanded=url['expanded_url'])
                    for url in tweet['entities']['urls']]
        mentions = None
        if 'entities' in tweet and 'mentions' in tweet['entities']:
            mentions = [Mention(start=m['start'], end=m['end'], username=m['username'], user_id=m['id'])
                        for m in tweet['entities']['mentions']]

        context_annotations = None
        if 'context_annotations' in tweet:
            context_annotations = [ContextAnnotation(domain_id=ca['domain']['id'], domain_name=ca['domain']['name'],
                                                     entity_id=ca['entity']['id'], entity_name=ca['entity']['name'])
                                   for ca in tweet['context_annotations']]

        tweet_obj = TwitterItemModel(
            twitter_id=str(tweet['id']),
            twitter_author_id=str(tweet['author_id']),
            text=tweet['text'],
            created_at=datetime.strptime(tweet['created_at'][:19], '%Y-%m-%dT%H:%M:%S'),
            language=tweet.get('lang'),
            conversation_id=tweet.get('conversation_id'),
            referenced_tweets=ref_tweets,
            context_annotations=context_annotations,
            latitude=latlon[0] if latlon else None,
            longitude=latlon[1] if latlon else None,
            hashtags=hashtags,
            mentions=mentions,
            urls=urls,
            cashtags=cashtags,
            retweet_count=tweet['public_metrics']['retweet_count'],
            reply_count=tweet['public_metrics']['reply_count'],
            like_count=tweet['public_metrics']['like_count'],
            quote_count=tweet['public_metrics']['quote_count'],
            user=user  # type: ignore[arg-type]
        )

        yield tweet_obj


@library.func(
    name='Twitter API data import',
    artefacts={},
    est_cpu_load=CPULoadClassification.LOW,
    # we may only keep this for the log, but it could be deleted right away
    recommended_lifetime=7,
    runtime_estimator=lambda n: n * 0.1,
    memory_estimator=lambda n: 200,
    tags=['twitter'])
async def import_twitter_api(tweet_api_pages: Artefact[JSONLSerializer[dict[str, Any]]],
                             project_id: str | None = None,
                             import_id: str | None = None,
                             ctx: TaskContext | None = None) -> None:
    """
    Import Tweets that were previously retrieved via the Twitter search API.
    We assume, that all options were turned on for the query to retain maximum information!

    `project_id` and `import_id` can be set to automatically populate the M2M tables
    and link the data to an import or project.

    **tweet_api_pages**
        An Artefact of a paged Twitter search API result
    **project_id**
        The project_id to connect these tweets to
    **import_id**
        The import_id to connect these tweets to
    """
    db_engine = get_db_engine_async()

    if ctx is None:
        ctx = TaskContext(logger=logging.getLogger('import_wos_file'),
                          target_directory=pathlib.Path.cwd(),
                          working_directory=pathlib.Path.cwd())

    ctx.logger.info(f'Importing tweets (Twitter API page formatted) from file: {tweet_api_pages.filename}')
    with JSONLSerializer[dict](filepath=tweet_api_pages.filename) as serializer:  # type: ignore[type-arg]
        for page in serializer.read():
            tweets = list(_api_page_to_tweets(page))  # type: ignore[arg-type]
            await import_tweets(tweets=tweets, import_id=import_id, project_id=project_id,
                                engine=db_engine)


@library.func(
    name='Twitter DB items import',
    artefacts={},
    est_cpu_load=CPULoadClassification.LOW,
    # we may only keep this for the log, but it could be deleted right away
    recommended_lifetime=7,
    runtime_estimator=lambda n: n * 0.1,
    memory_estimator=lambda n: 200,
    tags=['twitter'])
async def import_twitter_db(tweets: Artefact[JSONLSerializer[TwitterItemModel]],
                            project_id: str | None = None,
                            import_id: str | None = None,
                            ctx: TaskContext | None = None) -> None:
    """
    Import Tweets that are in the exact format of how tweets are stored in the database.
    We assume one JSON-encoded TwitterItemModel per line.

    `project_id` and `import_id` can be set to automatically populate the M2M tables
    and link the data to an import or project.

    **tweets**
        An Artefact of a paged Twitter search API result
    **project_id**
        The project_id to connect these tweets to
    **import_id**
        The import_id to connect these tweets to
    """
    db_engine = get_db_engine_async()

    if ctx is None:
        ctx = TaskContext(logger=logging.getLogger('import_wos_file'),
                          target_directory=pathlib.Path.cwd(),
                          working_directory=pathlib.Path.cwd())

    if tweets.filenames is not None:
        files = tweets.filenames
    elif tweets.filename is not None:
        files = [tweets.filename]
    else:
        raise AttributeError('No source file(s) specified!')

    for file in files:
        ctx.logger.info(f'Importing tweets (TwitterItemModel-formatted) from file: {file}')
        with JSONLSerializer[dict](filepath=file) as serializer:  # type: ignore[type-arg]
            for tweet in serializer.read():
                await import_tweet(tweet=TwitterItemModel.model_validate(tweet),
                                   import_id=import_id, project_id=project_id, engine=db_engine)
