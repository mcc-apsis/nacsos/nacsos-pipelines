import logging
from typing import Any
from time import sleep
from dataclasses import dataclass
from typing import TypedDict, Literal

from nacsos_data.models.pipeline import CPULoadClassification

from nacsos_pipes.common import library
from nacsos_pipes.common.task.artefacts import Artefact
from nacsos_pipes.common.disk.serializers import JSONLSerializer
from nacsos_pipes.executor.types import TaskContext


@dataclass
class ParamObj:
    sub_par1: str
    sub_par2: float


class DictParamObj(TypedDict):
    sub_par3: int
    sub_par4: bool


@library.func(
    name='This is a nicer function',
    est_cpu_load=CPULoadClassification.HIGH,
    memory_estimator=lambda n: 1024 * 1024 * (n ** 2),
    runtime_estimator=lambda n: 5 * n,
    recommended_lifetime=21,
    artefacts={},
)
async def hard_function(param_a: list[int],
                        param_b: str,
                        param_c: Literal['a', 'b'],
                        test_bool: bool,
                        test_f: float,
                        obj: ParamObj,
                        obj2: DictParamObj,
                        test_bool_def: bool = False,
                        ctx: TaskContext | None = None) -> None:
    """
    docstring test
    """
    logger = ctx.logger if ctx is not None else logging.getLogger()
    logger.debug(obj)
    logger.debug(obj2)
    logger.info(f'param_a: {param_a}')


@library.func(
    name='Test Function',
    artefacts={
        'tweets': Artefact[Any](filename='raw_tweets.jsonl',
                                serializer=JSONLSerializer,  # type: ignore[arg-type]
                                dtype=int)  # type: ignore[arg-type]
    })
async def testfunc(param_a: int,
                   param_b: str,
                   source_asset: Artefact[JSONLSerializer[list[str]]],
                   default_param: int = 5,
                   ctx: TaskContext | None = None) -> None:
    """
    docstring test
    """
    if ctx is None:
        raise RuntimeError('Missing context')

    logger = ctx.logger if ctx is not None else logging.getLogger()

    logger.debug('debug')
    logger.info('info')
    logger.warning('warning')
    logger.error('error')
    logger.fatal('fatal')
    logger.critical('critical')

    print(param_a)
    print(param_b)
    print(source_asset)
    print(ctx)
    print(default_param)

    with open(ctx.target_directory / 'test', 'w') as f:
        f.write('test')

    print('going to sleep')
    sleep(2)
    print('finished sleeping')


@library.func(
    name='This is a nice function',
    artefacts={
        'dummy': Artefact[Any](filename='dummyfile.jsonl',
                               serializer=JSONLSerializer,  # type: ignore[arg-type]
                               dtype=list[str])  # type: ignore[arg-type]
    })
async def tmpfunc(param_a: int, ctx: TaskContext | None = None) -> None:
    """
    docstring test
    """

    logger = ctx.logger if ctx is not None else logging.getLogger()
    logger.info(f'Whatever {param_a}')
