# !/usr/bin/env python3
from .common import get_logger

logger = get_logger('nacsos-pipes')

logger.info('Starting up server')
from .server import app  # noqa: E402


@app.on_event('startup')
async def init_system() -> None:
    logger.info('[startup hook] Loading library...')
    from .common import library
    library.reload()

    logger.info('[startup hook] Starting executor...')
    from .executor import executor
    executor.attach_loop()

    # await executor.run_task('tid1')
    # await executor.run_task('tid2')
    # await executor.run_task('tid3')
    # await executor.run_task('tid4')

    logger.info('[startup hook] Starting TaskQueue...')
    from .common.queue import task_queue
    await task_queue.on_startup()


@app.on_event('shutdown')
def clear_system() -> None:
    logger.info('[shutdown hook] Starting executor...')
    from .executor import executor
    executor.detach_loop()
