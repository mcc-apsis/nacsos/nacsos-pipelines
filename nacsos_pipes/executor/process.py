import traceback
import importlib
import asyncio
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Any, TypeVar

from ..common.disk.serializers import BaseSerializer
from ..common.logging import LogRedirector, get_file_logger
from ..common.task import TaskStatus
from ..common.task.artefacts import Artefact, is_obj_serialized_artefact

from .types import ExecutionInfo, TaskContext

T = TypeVar('T', bound=BaseSerializer[Any])


def resolve_kwarg(v: Any) -> Artefact[T] | Any:
    if is_obj_serialized_artefact(v):
        return Artefact(**v)
    return v


def task_executor(target_dir: str, working_dir: str | None = None) -> TaskStatus:
    target_directory = Path(target_dir)
    exec_info = ExecutionInfo.load(target_dir=target_directory)

    logger = get_file_logger(name=exec_info.function, out_file=target_directory / 'progress.log',
                             level='DEBUG', stdio=False)

    with TemporaryDirectory(dir=working_dir) as workdir, \
            LogRedirector(logger, level='INFO', stream='stdout'), \
            LogRedirector(logger, level='ERROR', stream='stderr'):

        kwargs: dict[str, Any] = {key: resolve_kwarg(val) for key, val in exec_info.params.items()}

        kwargs['ctx'] = TaskContext(
            target_directory=target_directory,
            working_directory=Path(workdir),
            logger=logger,
        )

        try:
            # Import the module
            module = importlib.import_module(exec_info.module, package=exec_info.package_path)
            module = importlib.reload(module)

            # Get the function from the string
            func = getattr(module, exec_info.function)

            # Do the actual function call
            if not asyncio.iscoroutinefunction(func):
                logger.info(f'Running {exec_info.function} with asyncio...')
                loop = asyncio.new_event_loop()
                loop.run_until_complete(func(**kwargs))
            else:
                logger.info(f'Running {exec_info.function} normally...')
                func(**kwargs)

            return TaskStatus.COMPLETED

        except (Exception, Warning) as e:
            # Oh no, something failed. Do some post-mortem logging
            tb = traceback.format_exc()
            logger.fatal(tb)
            logger.fatal(f'{type(e).__name__}: {e}')

            return TaskStatus.FAILED
