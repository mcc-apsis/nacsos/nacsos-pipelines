import asyncio
import logging
from asyncio import AbstractEventLoop
from functools import partial
from concurrent.futures import ProcessPoolExecutor
from concurrent.futures.process import BrokenProcessPool
from typing import Callable, Any

from nacsos_data.models.pipeline import SerializedArtefact
from nacsos_data.db.crud.pipeline import read_task_by_id

from ..common import settings, db_engine
from ..common.errors import \
    ProcessCancelled, \
    ProcessIncomplete, \
    ProcessNotDone, \
    TaskNotPendingWarning, \
    UnknownLibraryFunction, \
    UnknownTaskID
from ..common.task import TaskStatus

from ..common.task.library import library
from ..common.task.artefacts import \
    is_obj_artefact_reference, \
    is_obj_serialized_artefact, \
    is_obj_user_artefact, \
    Artefact, \
    ArtefactReference, \
    UserArtefact
from ..common.queue import task_queue as queue

from .process import task_executor
from .types import TaskContext, ExecutionInfo
from . import system

logger = logging.getLogger('nacsos-pipes.executor')


def resolve_kwarg(v: ArtefactReference | SerializedArtefact | dict[str, Any] | int | float | str) \
        -> dict[str, str | list[str]] | SerializedArtefact | int | float | str:
    if isinstance(v, ArtefactReference):
        return (v.as_artefact_sync()).as_dict()
    if is_obj_artefact_reference(v):
        # assert type(v) == SerializedArtefactReference
        return (ArtefactReference(**v).as_artefact_sync()).as_dict()  # type: ignore[arg-type]
    if is_obj_serialized_artefact(v):
        # assert type(v) == SerializedArtefact
        return Artefact(**v).as_dict()  # type: ignore[arg-type]
    if is_obj_user_artefact(v):
        # assert type(v) == SerializedUserArtefact
        return UserArtefact(**v).as_artefact().as_dict()  # type: ignore[arg-type]
    return v


class Executor:
    def __init__(self) -> None:
        self._loop: AbstractEventLoop | None = None
        # set_start_method('spawn') # FIXME: somehow get spawn to work, supposedly much more efficient.
        self._pool = ProcessPoolExecutor(max_workers=settings.EXECUTOR.MAX_PROCESSES)
        self._processes: dict[str, asyncio.Future[Any]] = {}

    def get_num_free_processes(self) -> int:
        """
        Checks how many processes are currently running and
        calculates how many more the settings would allow to run.
        """
        return settings.EXECUTOR.MAX_PROCESSES - len(self._processes)

    def attach_loop(self) -> None:
        self._loop = asyncio.get_running_loop()

    def detach_loop(self) -> None:
        for key, proc in self._processes.items():
            logger.warning(f'Cancelling process {key} before detaching executor loop.')
            proc.cancel('SHUTDOWN')
        else:
            logger.warning('Tried to close running processes, none found.')
        if self._pool:
            logger.warning('Shutting down executor pool.')
            self._pool.shutdown(wait=False, cancel_futures=True)
        if self._loop:
            logger.warning('Closing executor loop.')
            try:
                self._loop.close()
            except RuntimeError as e:
                logger.warning(getattr(e, 'message', repr(e)))
            try:
                self._loop.stop()
            except RuntimeError as e:
                logger.warning(getattr(e, 'message', repr(e)))

    def run_task(self, task_id: str, force: bool = False) -> None:
        if self._loop is None:
            raise RuntimeError('No event loop (yet)!')

        try:
            # Ensure target directory exists
            (settings.EXECUTOR.target_dir / str(task_id)).mkdir(parents=True, exist_ok=True)

            # Gather all the necessary information
            task = read_task_by_id(task_id=task_id, engine=db_engine)
            if task is None:
                raise UnknownTaskID(f'Task not found in database | ID: {task_id}')

            if not force and task.status != 'PENDING':
                raise TaskNotPendingWarning(f'This task is not `PENDING` anymore, it is "{task.status}"')

            func_info = library.find_by_name_sync(task.function_name)
            if func_info is None:
                raise UnknownLibraryFunction(f'Function by the name "{task.function_name}" '
                                             f'not found in the library for task {task_id}')

            # Resolve function kwargs
            kwargs = {
                key: resolve_kwarg(value)  # type: ignore[arg-type]  # FIXME
                for key, value in task.params.items()
            } if task.params and type(task.params) == dict else {}

            # Construct and write execution info file
            exec_info = ExecutionInfo(
                package_path=library.package,
                filepath=func_info.filepath,
                module=func_info.module,
                function=func_info.function,
                params=kwargs)
            exec_info.store(target_dir=settings.EXECUTOR.target_dir / task_id)

            # Put the task (wrapped in the `task_executor` function) in the processing pool
            result = self._loop.run_in_executor(executor=self._pool,
                                                func=partial(task_executor,
                                                             target_dir=str(settings.EXECUTOR.target_dir / task_id),
                                                             working_dir=str(settings.EXECUTOR.WORKING_DIR)))
            # Store the `Future` to keep track of it for later reference
            self._processes[task_id] = result
            # Tell the `Future` where to go once the process stopped
            result.add_done_callback(self.resolve_completed(task_id))

            # Tell the queue that we are now running this task
            # This is done last, because anything that breaks before here was probably not the task's fault
            # (and we didn't confuse the queue for nothing)
            queue.update_task_status(task_id=task_id, status=TaskStatus.RUNNING)
        except BrokenProcessPool as e:
            logger.warning(f'Failed to run task due to a problem with the process pool | ID: {task_id}')
            logger.exception(f'{e.__class__.__name__}: {e}')
            queue.update_task_status(task_id=task_id, status=TaskStatus.FAILED)
        except Exception as e:
            logger.warning(f'Did not try to run task due to an exception | ID: {task_id}')
            logger.exception(f'{e.__class__.__name__}: {e}')
            queue.update_task_status(task_id=task_id, status=TaskStatus.FAILED)

    def resolve_completed(self, task_id: str) -> Callable[[asyncio.Future[TaskStatus]], None]:
        """
        This function handles the callback when a process stopped working.
        It does some tests to the `Future` to see if the process stopped
        due to finishing a Task successfully or otherwise.
        Based on that, it updates the queue/API accordingly.
        It's wrapped, so we get a handle on the `task_id`.
        """

        def resolve(result: asyncio.Future[TaskStatus]) -> None:
            logger.info(f'Task is done, resolving outcome | ID: {task_id}')

            # Clear up our process memory in any case
            del self._processes[task_id]

            try:
                if result.cancelled():
                    raise ProcessCancelled('Process was cancelled before finishing.')
                if not result.done():
                    raise ProcessNotDone('Process is not actually "done".')

                status: TaskStatus = result.result()
                if status == 'COMPLETED':
                    queue.update_task_status(task_id=task_id, status=TaskStatus.COMPLETED)
                    logger.info(f'Task successfully completed! | ID {task_id}')
                else:
                    raise ProcessIncomplete(f'Task did not complete and returned with status "{status}"')

            except Exception as e:
                queue.update_task_status(task_id=task_id, status=TaskStatus.FAILED)
                logger.exception(f'{e.__class__.__name__}: {e}')

                # TODO: Should we delete the target directory or keep it when a task failed?
                #       Partial results may still be helpful and it contains logs.

        return resolve


executor = Executor()

__all__ = ['executor', 'TaskContext', 'ExecutionInfo', 'system']
