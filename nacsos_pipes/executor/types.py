import json
import logging
from pathlib import Path
from dataclasses import dataclass, asdict
from typing import Any


@dataclass
class TaskContext:
    target_directory: Path
    working_directory: Path
    logger: logging.Logger


@dataclass
class ExecutionInfo:
    package_path: str  # absolute path to root directly of nacsos_library
    filepath: str  # absolute path to the script file containing the function to be called
    module: str  # module containing the function
    function: str  # function to be called
    # FIXME be more specific than `Any`
    params: dict[str, Any]  # the function call parameters (unrolled and with absolute paths if necessary)

    def store(self, target_dir: Path) -> None:
        payload = json.dumps(asdict(self), indent=2)
        with open(target_dir / '.exec_info.json', 'w') as f_out:
            f_out.write(payload)

    @classmethod
    def load(cls, target_dir: Path) -> 'ExecutionInfo':
        with open(target_dir / '.exec_info.json', 'r') as f_in:
            payload = json.load(f_in)
            return ExecutionInfo(**payload)
