import psutil
from ..common import settings


# Use this to set the window to look at when checking avg. CPU loads
# 0 = 1min | 1 = 5min | 2 = 15min
CPU_LOAD_WINDOW = 1


def get_memory_headroom() -> int:
    """
    Compute and return the free memory left on the system
    excluding the courtesy memory we want to leave for the system
    Returned value in MB.
    """
    return int(psutil.virtual_memory().free / 1024 / 1024) - settings.EXECUTOR.MIN_FREE_RAM


def get_num_free_cores() -> float:
    """
    Approximates the CPU core capacity left on the system
    """
    return psutil.cpu_count() - (psutil.getloadavg()[CPU_LOAD_WINDOW] / 100)


def system_capacity_available() -> bool:
    """
    Based on the config settings, this function checks all the configured
    limits and returns a boolean telling whether there is still capacity left.
    """
    # Get current memory utilisation
    memory = psutil.virtual_memory()

    if get_memory_headroom() <= 0:
        # Not enough free RAM left
        return False

    if (memory.used / memory.total) > settings.EXECUTOR.MAX_RAM_USAGE:
        # Overall system RAM above threshold
        return False

    # Get avg. load of last few minutes (normalised by num cores)
    if (psutil.getloadavg()[CPU_LOAD_WINDOW] / psutil.cpu_count()) > settings.EXECUTOR.MAX_CPU_USAGE:
        # CPU was used too much in the last few minutes
        return False

    # Yes, there's capacity for more!
    return True
