from pathlib import Path
from typing import Any
import secrets
import json
import toml
import os
from importlib import import_module

from pydantic_settings import SettingsConfigDict, BaseSettings
from pydantic.networks import PostgresDsn
from pydantic import field_validator, ValidationInfo, BaseModel, model_validator, computed_field


class PipelinesAPIConfig(BaseModel):
    PORT: int = 8000  # The port where the API will be accessible.
    HOST: str = 'localhost'  # The host where the API will be accessible.
    SSL: bool = False  # set to true to assume we are using HTTPS
    DEBUG_MODE: bool = False  # set this to true in order to get more detailed logs
    WORKERS: int = 2  # number of worker processes
    HASH_ALGORITHM: str = 'HS256'
    SECRET_KEY: str = secrets.token_urlsafe(32)
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 8  # = 8 days

    OPENAPI_FILE: str = '/openapi.json'  # absolute URL path to openapi.json file
    OPENAPI_PREFIX: str = ''  # see https://fastapi.tiangolo.com/advanced/behind-a-proxy/
    ROOT_PATH: str = ''  # see https://fastapi.tiangolo.com/advanced/behind-a-proxy/

    HEADER_CORS: bool = False  # set to true to set up CORS middleware
    CORS_ORIGINS: list[str] = []  # list of trusted origins

    @property
    def api_url(self) -> str:
        protocol = 'https' if self.SSL else 'http'
        return f'{protocol}://{self.HOST}:{self.PORT}/api'

    @field_validator('CORS_ORIGINS', mode='before')
    @classmethod
    def assemble_cors_origins(cls, v: str | list[str]) -> str | list[str]:
        if isinstance(v, str) and not v.startswith('['):
            return [i.strip() for i in v.split(',')]
        if isinstance(v, str) and v.startswith('['):
            ret = json.loads(v)
            if type(ret) is list:
                return ret
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v)


class PipelinesExecutorConfig(BaseModel):
    DATA_PATH: Path = Path('.nacsos')  # Where results and the job database will be stored.
    WORKING_DIR: Path = Path('.nacsos/tmp')  # Directory for temporary files
    MAX_PROCESSES: int = 4  # Max. number of parallel processes/workers
    MAX_CPU_USAGE: float = .9  # If max CPU utilization (in %) is reached, no more new tasks will be started
    MAX_RAM_USAGE: float = .8  # If max RAM utilization (in %) is reached, no more new tasks will be started
    MIN_FREE_RAM: int = 2000  # If less free RAM (in ḾB) than this is left on system, no more new tasks will be started

    CORE_THRESHOLD_VHIGH: int = 3
    CORE_THRESHOLD_HIGH: int = 3
    CORE_THRESHOLD_MEDIUM: int = 2
    CORE_THRESHOLD_LOW: int = 1
    CORE_THRESHOLD_MINIMAL: int = 0

    @property
    def target_dir(self) -> Path:
        return (self.DATA_PATH / 'artefacts').resolve()

    @property
    def user_data_dir(self) -> Path:
        return (self.DATA_PATH / 'user_data').resolve()

    @model_validator(mode='before')
    @classmethod
    def fix_paths(cls, data: Any) -> Any:

        def ensure_path(key: str) -> Path:
            v = data.get(key)
            if isinstance(v, str):
                path = Path(v)
            elif isinstance(v, Path) and v.is_absolute():
                path = v
            elif isinstance(v, Path):
                path = (Path.cwd() / Path(v))
            else:
                raise ValueError(f'Invalid path for {key}: {v}')
            path = path.resolve()
            path.mkdir(parents=True, exist_ok=True)
            return path

        data['DATA_PATH'] = ensure_path('DATA_PATH')
        data['WORKING_DIR'] = ensure_path('WORKING_DIR')
        return data


class DatabaseConfig(BaseModel):
    HOST: str = 'localhost'  # host of the db server
    PORT: int = 5432  # port of the db server
    USER: str = 'nacsos'  # username for the database
    PASSWORD: str = 'secr€t_passvvord'  # password for the database user
    DEBUG: bool = False  # set to true to log queries
    DATABASE: str = 'nacsos_core'  # name of the database
    SCHEMA: str = 'public'
    SCHEME: str = 'postgresql'

    @computed_field  # type: ignore[misc]
    @property
    def CONNECTION_STR(self) -> PostgresDsn:
        return PostgresDsn.build(
            scheme=self.SCHEME,
            username=self.USER,
            password=self.PASSWORD,
            host=self.HOST,
            port=self.PORT,
            path=f'/{self.DATABASE}',
        )


class PipelinesLibraryConfig(BaseModel):
    LIBRARY_PATH: str = '../nacsos_library'  # Path to the library package
    LIBRARY: Any | None = None

    @field_validator('LIBRARY', mode='before')
    def load_library_module(cls, v: str | None, info: ValidationInfo) -> Any:
        assert info.config is not None

        module_path: str | None = info.data.get('LIBRARY_PATH')
        if module_path is not None:
            return import_module(module_path, package='nacsos_pipelines')
        else:
            raise AttributeError('Missing library path!')


class PIKConnectorConfig(BaseModel):
    USERNAME: str | None = None  # Your username for connecting to the PIK cluster via SSH.
    HOST: str = 'cluster.pik-potsdam.de'  # PIK cluster hostname
    # path on the PIK cluster where Python libraries are uploaded and data is stored:
    DATA_PATH: Path = Path('~/.nacsos')
    SKIP_SETUP: bool = False  # skip setting up conda and pip
    REFRESH_INTERVAL: int = 1  # number of seconds to wait before next status call to cluster
    REFRESH_LOG_FREQUENCY: int = 10  # reduces log frequency by only printing every n-th time


class Settings(BaseSettings):
    API: PipelinesAPIConfig
    EXECUTOR: PipelinesExecutorConfig
    PIK: PIKConnectorConfig
    LIBRARY: PipelinesLibraryConfig

    CORE_API_URL: str = 'http://localhost:8081/api'  # no trailing slash!
    DB_CORE: DatabaseConfig
    DB_OPENALEX: DatabaseConfig
    OPENALEX_URL: str = 'http://localhost:8983/solr/openalex'

    LOG_CONF_FILE: str = 'config/logging.conf'
    LOGGING_CONF: dict[str, Any] | None = None

    @field_validator('LOGGING_CONF', mode='before')
    @classmethod
    def read_logging_config(cls, v: dict[str, Any] | None, info: ValidationInfo) -> dict[str, Any]:
        assert info.config is not None

        if isinstance(v, dict):
            return v
        filename = info.data.get('LOG_CONF_FILE', None)
        if filename is not None:
            with open(filename, 'r') as f:
                ret = toml.loads(f.read())
                if type(ret) is dict:
                    return ret
        raise ValueError('Logging config invalid!')

    model_config = SettingsConfigDict(case_sensitive=True, env_nested_delimiter='__')


conf_file = os.environ.get('NACSOS_CONFIG', 'config/default.env')
print(f'Loading config from: {conf_file}')
settings = Settings(_env_file=conf_file, _env_file_encoding='utf-8')

__all__ = ['settings']
