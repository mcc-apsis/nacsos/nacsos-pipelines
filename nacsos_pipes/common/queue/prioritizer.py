from nacsos_data.models.pipeline import TaskModel
from nacsos_data.db.crud.pipeline import read_task_statuses

from ...common.db import db_engine
from .. import settings


def dependencies_fulfilled(task: TaskModel) -> bool:
    """
    This function returns true iff this task has
    no dependencies or all it's dependencies are fulfilled.
    """
    if task.dependencies is None:
        return True

    tasks = read_task_statuses(task_ids=task.dependencies, engine=db_engine)
    for task_ in tasks:
        if task_.status != 'COMPLETED':
            return False

    return True


def sufficient_capacity(task: TaskModel) -> bool:
    from ...executor import system
    # Does this task fit in memory?
    if task.est_memory is not None and task.est_memory > system.get_memory_headroom():
        return False

    # Does the CPU have enough capacity to handle this task?
    if task.est_cpu_load is not None:
        num_free_cores = system.get_num_free_cores()
        if task.est_cpu_load == 'VHIGH' and num_free_cores < settings.EXECUTOR.CORE_THRESHOLD_VHIGH:
            return False
        if task.est_cpu_load == 'HIGH' and num_free_cores < settings.EXECUTOR.CORE_THRESHOLD_HIGH:
            return False
        if task.est_cpu_load == 'MEDIUM' and num_free_cores < settings.EXECUTOR.CORE_THRESHOLD_MEDIUM:
            return False
        if task.est_cpu_load == 'LOW' and num_free_cores < settings.EXECUTOR.CORE_THRESHOLD_LOW:
            return False
        if task.est_cpu_load == 'MINIMAL' and num_free_cores < settings.EXECUTOR.CORE_THRESHOLD_MINIMAL:
            return False

    return True

# TODO function to check which tasks would still fit in system resources
# TODO function to check how long a task has been waiting
# TODO function to check the tasks/user currently running
