import logging
from typing import Any, Sequence
from uuid import uuid4
from datetime import datetime, timedelta

from sqlalchemy import select
from sqlalchemy.orm import Session

from nacsos_data.db.schemas import Task, Import
from nacsos_data.models.pipeline import TaskModel, TaskStatus, SubmittedTask, compute_fingerprint
from nacsos_data.db.crud.pipeline import \
    query_tasks, \
    reset_failed, \
    read_tasks_by_fingerprint, \
    upsert_task, \
    update_status

from ..errors import UnknownLibraryFunction, SameFingerprintWarning, InvalidTaskInstance
from ..task.artefacts import is_obj_artefact_reference
from ..task.library import library
from ..events import eventbus, Event
from ...common.db import db_engine
from . import prioritizer

logger = logging.getLogger('nacsos-pipes.queue')


class TaskQueue:
    def __init__(self) -> None:
        # Hook relevant events that can lead to possible task execution
        eventbus.on(Event.TaskChangedStatus, self.maybe_execute_next)
        eventbus.on(Event.TaskChangedStatus, self.update_nacsos_core)
        eventbus.on(Event.NewTaskReceived, self.maybe_execute_next)

    def maybe_execute_next(self, **kwargs: Any) -> None:
        """
        This is the core function of the `TaskQueue`.
        When called, it updates the order of tasks in the queue,
        checks which can actually run, and if resources permit,
        send a task off to the executor.
        """
        from ...executor import executor, system  # imported down here to prevent circular import

        logger.info('Received `startup`, `TaskChangedStatus*`, or `NewTaskReceived` Event, '
                    'checking if there are tasks to run.')
        logger.debug(f'Free processes: {executor.get_num_free_processes()} | '
                     f'Capacity available: {system.system_capacity_available()}')

        # Only proceed if we'd even have capacity available to start running a task
        if executor.get_num_free_processes() > 0 and system.system_capacity_available():
            # Get all tasks marked as pending
            pending_tasks = query_tasks(engine=db_engine, status='PENDING')
            logger.debug(f'Currently {len(pending_tasks or [])} pending tasks in the queue.')

            if pending_tasks is not None:
                # Drop those, that can not run at the moment anyway
                runnable_tasks = [task for task in pending_tasks if prioritizer.dependencies_fulfilled(task)]
                logger.debug(f'Currently {len(runnable_tasks)} tasks with fulfilled dependencies in the queue.')
                runnable_tasks = [task for task in runnable_tasks if prioritizer.sufficient_capacity(task)]
                logger.debug(f'Currently {len(runnable_tasks)} tasks with sufficient system capacity in the queue.')

                # TODO: Actually rank tasks, e.g. tasks/user, task waiting time, ...
                ranked_tasks = runnable_tasks

                logger.debug(f'Found {len(ranked_tasks)} ranked and runnable tasks.')
                logger.info(f'`TaskQueue` will tell executor to run task {ranked_tasks[0].task_id}')
                if len(ranked_tasks) > 0 and ranked_tasks[0].task_id is not None:
                    executor.run_task(str(ranked_tasks[0].task_id))

    async def force_execute(self, task_id: str) -> None:
        from ...executor import executor
        executor.run_task(task_id=task_id, force=True)

    async def on_startup(self) -> None:
        # In case the previous exit wasn't clean, set all previously running tasks to failed.
        # TODO: Should this also entail the deletion of partial results in target directories?
        reset_failed(engine=db_engine)
        self.maybe_execute_next()

    def _clear_orphans(self) -> None:
        """
        This method iterates the list of pending tasks in the database
        and looks for tasks that will never be able to run (e.g. due to failed dependency).
        It is not critical to run this often, but in order to keep the
        task backlog clean, it should run every now and then (or on `Event.TaskStatusChanged_failed`?).
        """
        # TODO
        # Note, that silently deleting tasks may not be optimal.
        # Maybe introduce an "ORPHANED" state that is set back to PENDING when the dependency is rescheduled?
        pass

    def _derive_dependencies(self, task: SubmittedTask | TaskModel) -> list[str] | None:
        if task.params is None or type(task.params) != dict:
            return None

        dependencies = []
        for key, value in task.params.items():
            if is_obj_artefact_reference(value):
                dependencies.append(value['task_id'])  # type: ignore

        if len(dependencies) > 0:
            return dependencies
        return None

    async def add_task(self, task: SubmittedTask | TaskModel, check_fingerprint: bool = False) -> str | None:
        logger.info(f'Adding new task to queue: {task.function_name} (ID: {task.task_id})')

        func_info = await library.find_by_name(task.function_name)
        if func_info is None:
            logger.warning(f'Did not find "{task.function_name} in library.')
            raise UnknownLibraryFunction(f'Did not find "{task.function_name} in library.')

        if isinstance(task, SubmittedTask):
            task_dict = task.dict()
            task_dict['fingerprint'] = compute_fingerprint(task.function_name, params=task.params)
            forced_dependencies: list[str] | None = \
                [str(d) for d in task.forced_dependencies] \
                    if task.forced_dependencies else None
            task = TaskModel.parse_obj(task_dict)

            if task.task_id is None:
                task.task_id = uuid4()

            task.time_finished = datetime.now()
            task.dependencies = self._derive_dependencies(task)
            if forced_dependencies:
                if not task.dependencies:
                    task.dependencies = forced_dependencies
                else:
                    task.dependencies += forced_dependencies

            # FIXME: fill these values
            task.est_memory = None  # func_info.memory_estimator()
            task.est_runtime = None  # func_info.runtime_estimator()
            if type(func_info.recommended_lifetime) == int:
                task.rec_expunge = datetime.now() + timedelta(days=func_info.recommended_lifetime)
            else:
                task.rec_expunge = None
            task.est_cpu_load = func_info.est_cpu_load

        elif isinstance(task, TaskModel):
            derived_dependencies: set[str] = set(self._derive_dependencies(task) or [])
            defined_dependencies: set[str] = set([str(d) for d in (task.dependencies or [])])
            merged_dependencies: list[str] = list(derived_dependencies.union(defined_dependencies))
            task.dependencies = merged_dependencies if len(merged_dependencies) > 0 else None
        else:
            raise InvalidTaskInstance('Did not receive a valid `Task` instance!')

        if check_fingerprint:
            fingerprint_collisions = read_tasks_by_fingerprint(fingerprint=task.fingerprint, engine=db_engine)
            if len(fingerprint_collisions) > 0:
                conflicts = [str(t.task_id) for t in fingerprint_collisions]
                raise SameFingerprintWarning('Some other task with the same fingerprint already exists!\n'
                                             f'Submitted task: {task.task_id}\n'
                                             f'Conflicting tasks: {", ".join(conflicts)}')

        task = TaskModel.parse_obj(task)
        upsert_task(task, engine=db_engine)
        await eventbus.emit_async(Event.NewTaskReceived)
        return str(task.task_id)

    def cancel_task(self, task_id: str) -> None:
        # TODO
        raise NotImplementedError()

    def remove_task(self, task_id: str) -> None:
        # TODO
        raise NotImplementedError()
        # task = TaskDB.from_db(task_id=task_id)
        # TaskDB.parse_obj(task).delete()
        # os.rmdir()

    async def add_tasks(self, tasks: Sequence[SubmittedTask | TaskModel], check_fingerprints: bool = False) \
            -> list[str | None]:
        return [await self.add_task(task, check_fingerprint=check_fingerprints) for task in tasks]

    def update_task_status(self, task_id: str, status: TaskStatus) -> None:
        logger.debug(f'Received status update ({status}) for task {task_id}')

        # update the database and tell the world
        update_status(task_id=task_id, status=status, engine=db_engine)
        if status == 'PENDING':
            eventbus.emit(Event.TaskChangedStatus_pending, task_id=task_id, status=status)
        elif status == 'RUNNING':
            eventbus.emit(Event.TaskChangedStatus_running, task_id=task_id, status=status)
        elif status == 'COMPLETED':
            eventbus.emit(Event.TaskChangedStatus_completed, task_id=task_id, status=status)
        elif status == 'FAILED':
            eventbus.emit(Event.TaskChangedStatus_failed, task_id=task_id, status=status)
        elif status == 'CANCELLED':
            eventbus.emit(Event.TaskChangedStatus_cancelled, task_id=task_id, status=status)

    def update_nacsos_core(self, task_id: str, status: TaskStatus) -> None:
        logger.info('Checking if the task needs some follow-up action in NACSOS-core')

        with db_engine.session() as session:  # type: Session
            task = session.scalars(select(Task).where(Task.task_id == task_id)).one_or_none()
            if task is not None:
                # if the task has information on `import_id`, we should update that
                if task.params is not None:
                    import_id: str | None = task.params.get('import_id', None)
                    if import_id is not None:
                        logger.debug('Seems like NACSOS-core should have a related import for this task.')
                        import_orm: Import | None = session.scalars(select(Import)
                                                                    .where((Import.import_id == import_id) |
                                                                           (Import.pipeline_task_id == import_id))
                                                                    ).one_or_none()
                        if import_orm is None:
                            logger.debug('Did not find anything, never mind.')
                        else:
                            logger.info(f'Updating NACSOS Import(id={import_orm.import_id}).')

                            if status == 'RUNNING':
                                import_orm.time_started = datetime.now()
                            elif status == 'COMPLETED':
                                import_orm.time_finished = datetime.now()
                            elif status == 'FAILED':
                                import_orm.time_finished = datetime.now()
                            elif status == 'CANCELLED':
                                import_orm.time_finished = datetime.now()
                            elif status == 'PENDING':
                                import_orm.time_created = datetime.now()
                                import_orm.time_started = None
                                import_orm.time_finished = None

                            session.commit()


task_queue = TaskQueue()

__all__ = ['task_queue']
