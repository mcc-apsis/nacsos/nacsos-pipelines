from pathlib import Path
from typing import get_args, TypeVar, Generic, Type, Any, TYPE_CHECKING
from types import GenericAlias
from inspect import isclass
from pydantic import ConfigDict, BaseModel, field_validator

from nacsos_data.db.crud.pipeline import read_task_by_id
from nacsos_data.models.pipeline import SerializedArtefact, TaskModel

from ...common.db import db_engine
from ..config import settings
from ..disk import serializers
from ..errors import LibraryFunctionNotFoundError

if TYPE_CHECKING:
    from .library import FunctionInfo

SerializerT = TypeVar('SerializerT', bound=serializers.BaseSerializer[Any])


class Artefact(BaseModel, Generic[SerializerT]):
    """
    An Artefact is a reference to a file. A function indicating that it will produce an Artifact
    essentially just "promises" to do so including a reference to how it's serialised and how it will call the file.
    In the context of TaskLibrary.funct(artefact=...) the `filename` parameter is to be understood as a relative
    path reference to the directory the Task execution will be referred to.
    During runtime, the `filename` parameter contains the actual resolved absolute path.
    """

    # The serializer used to read/write the data
    serializer: str
    # The datatype captured by the serializer
    dtype: str
    # The filename this artefact is stored at
    # In the context of a function info parameter, this is relative to the target directory,
    # during runtime this should refer to the absolute path.
    filename: str | None = None
    # An artefact may also refer to a group of files.
    # In the context of a function info parameter, this is a glob-style string relative to the target directory,
    # during runtime this should be the resolved glob with absolute filepaths.
    filenames: str | list[str] | None = None

    # Indicates whether built-in filename resolver was already called
    fn_resolved: bool = False
    model_config = ConfigDict(arbitrary_types_allowed=True)

    @field_validator('serializer', mode='before')
    @classmethod
    def serializer2str(cls,
                       v: Type[serializers.BaseSerializer[Any]] | serializers.BaseSerializer[Any] | str) -> str | None:
        if isinstance(v, str) and hasattr(serializers, v):
            return v
        if isclass(v) and issubclass(v, serializers.BaseSerializer):
            return v.__name__
        if isinstance(v, serializers.BaseSerializer):
            return str(v.__class__.__name__)
        raise ValueError(f'Invalid serializer {v}')

    @field_validator('dtype', mode='before')
    @classmethod
    def dtype2str(cls, v: Any) -> str:
        if isinstance(v, str):
            return v
        if hasattr(v, '__name__'):
            return v.__name__
        if hasattr(v, '__class__') and hasattr(v.__class__, '__name__'):
            return v.__class__.__name__
        return str(v)

    @field_validator('filename', mode='before')
    @classmethod
    def filename2str(cls, v: Any) -> str | None:
        if v is None:
            return v
        if isinstance(v, str):
            return v
        if isinstance(v, Path):
            return str(v)
        raise ValueError(f'Invalid filename {type(v)}')

    @classmethod
    def type2artefact(cls, tp: Type[Any]) -> 'Artefact[Any]':
        serializer = get_args(tp)[0]
        dtype = get_args(serializer)[0]
        if type(dtype) in [str, dict, int, float]:
            return Artefact(serializer=serializer.__name__, dtype=dtype.__name__)
        elif isclass(dtype) and hasattr(dtype, '__class__') and issubclass(dtype.__class__, BaseModel):
            return Artefact(serializer=serializer.__name__, dtype=dtype.__name__)
        else:
            return Artefact(serializer=serializer.__name__, dtype=str(dtype))

    @classmethod
    def try_generic_alias_serialisation(cls, tp: Type[Any]) -> Type[Any] | 'Artefact[Any]':
        if not tp or type(tp) is not GenericAlias:
            return tp
        if hasattr(tp, '__origin__') and tp.__origin__ == cls:
            return cls.type2artefact(tp)
        return tp

    @classmethod
    def is_artefact_type(cls, tp: Type[Any]) -> bool:
        """
        Checks whether the type (`tp`) passed to this method is a valid
        `Artefact` type or not. This is needed, because python does not directly expose the type
        when `Artefact` is typed:

        ```
        tp = Artefact[JSONLSerializer[list[int]]]

        str(tp) == 'nacsos_pipes.common.task.artefacts.Artefact[nacsos_pipes.common.disk.serializers.jsonl.JSONLSerializer[list[int]]]'
        type(tp) == <class 'typing._GenericAlias'>
        tp.__origin__ == <class 'nacsos_pipes.common.task.artefacts.Artefact'>
        tp.__name__ == 'Artefact'

        tp_untyped = Artefact
        type(tp_untyped) == <class 'type'>
        str(tp) == "<class 'nacsos_pipes.common.task.artefacts.Artefact'>"
        ```
        """
        return (type(tp) is GenericAlias and hasattr(tp, '__origin__') and tp.__origin__ == cls) or tp == Artefact

    def as_dict(self) -> SerializedArtefact:
        return {
            'serializer': self.serializer,
            'dtype': self.dtype,
            'filename': self.filename,
            'filenames': self.filenames
        }

    @property
    def is_resolved(self) -> bool:
        """
        Indicates whether the path is fully resolved already,
        meaning it points to an absolute file location.
        """
        if self.filename is not None:
            return self.fn_resolved or Path(self.filename).is_absolute()
        if self.filenames is not None:
            return self.fn_resolved or all(Path(fn).is_absolute() for fn in self.filenames)
        return False

    def resolve_path(self, task_id: str) -> None:
        """
        Given the `task_id`, this function will resolve the internal `filename`
        to the appropriate absolute path in-place.
        """
        if self.filename is None and self.filenames is None:
            raise AttributeError('Filename has to be set')
        if self.is_resolved:
            raise AttributeError('Already resolved the path')

        if self.filename is not None:
            self.filename = str(settings.EXECUTOR.target_dir / task_id / self.filename)
        elif type(self.filenames) is str:
            self.filenames = [str(fn) for fn in (settings.EXECUTOR.target_dir / task_id).glob(self.filenames)]
        # elif type(self.filenames) == list:
        #     self.filenames = self.filenames
        self.fn_resolved = True

    @classmethod
    def _ensure_task(cls, task_id: str) -> 'TaskModel':
        task = read_task_by_id(task_id=task_id, engine=db_engine)

        if not task:
            raise ValueError(f'No task found for {task_id}!')
        return task

    @classmethod
    def _from_reference(cls, task: 'TaskModel', library_function: 'FunctionInfo', artefact_name: str) -> 'Artefact[Any]':
        if not library_function:
            raise ValueError(f'No function by the name of "{task.function_name}" found in the library!')

        artefact = library_function.artefacts.get(artefact_name, None)
        if not artefact:
            raise ValueError(f'No artefact by the name of "{artefact_name}" found in task! '
                             f'({library_function.artefacts})')
        if not artefact.filename or not artefact.filenames:
            raise ValueError(f'Data integrety error, no filename(s) found for "{artefact_name}" found in task! '
                             f'({library_function.artefacts})')
        if task.task_id is None:
            raise ValueError('`task_id` is not specified!')

        filename = None
        filenames = None
        if artefact.filename is not None:
            filename = str((settings.EXECUTOR.target_dir / str(task.task_id) / artefact.filename).resolve())
        else:
            filenames = [str(fn) for fn in (settings.EXECUTOR.target_dir / str(task.task_id)).glob(artefact.filenames)]

        return Artefact(
            filename=filename,
            filenames=filenames,
            serializer=artefact.serializer,
            dtype=artefact.dtype,
            fn_resolved=True
        )

    @classmethod
    def from_reference_sync(cls, task_id: str, artefact_name: str) -> 'Artefact[Any]':
        """
        This performs a database lookup to resolve the function corresponding to the `task_id`.
        With that, it checks the `library` to find the unresolved `Artefact` that function promised to produce.
        The local filename there (along with serializer and typy information) is used to construct
        a resolved artefact that is returned.
        """
        from .library import library
        task = cls._ensure_task(task_id=task_id)
        library_function = library.find_by_name_sync(task.function_name)

        if library_function is None:
            raise LibraryFunctionNotFoundError(f'Library function not found by the name of {task.function_name}')

        return cls._from_reference(task=task, library_function=library_function, artefact_name=artefact_name)

    @classmethod
    async def from_reference(cls, task_id: str, artefact_name: str) -> 'Artefact[Any]':
        """
        This performs a database lookup to resolve the function corresponding to the `task_id`.
        With that, it checks the `library` to find the unresolved `Artefact` that function promised to produce.
        The local filename there (along with serializer and typy information) is used to construct
        a resolved artefact that is returned.
        """
        from .library import library

        task = cls._ensure_task(task_id=task_id)
        library_function = await library.find_by_name(task.function_name)

        if library_function is None:
            raise LibraryFunctionNotFoundError(f'Library function not found by the name of {task.function_name}')

        return cls._from_reference(task=task, library_function=library_function, artefact_name=artefact_name)

    @property
    def serializer_(self) -> str:
        """
        Returns the actual BaseSerializer class as resolved from the internally stored string.
        """
        if self.serializer and hasattr(serializers, self.serializer):
            return getattr(serializers, self.serializer)
        raise AttributeError(f'The serializer {self.serializer} does not exist')


class ArtefactReference(BaseModel):
    """
    This is an (unresolved) reference to an artefact of some function.
    This model is used to save Task parameters in the database and to communicate to/from the frontend.

    Based on the information provided, the abstract reference can be resolved to a location on disk
    where the file is (if respective task is completed) or will be.
    """
    task_id: str  # the task_id assigned to a function execution
    artefact: str  # the artefact key as indicated in library.func(artefacts=...)

    async def as_artefact(self) -> Artefact[Any]:
        return await Artefact.from_reference(task_id=self.task_id, artefact_name=self.artefact)

    def as_artefact_sync(self) -> Artefact[Any]:
        return Artefact.from_reference_sync(task_id=self.task_id, artefact_name=self.artefact)


class UserArtefact(BaseModel):
    """
    This is a reference to a file (or list of files) that a user created (typically by uploading it).
    All paths are relative to `user_data_dir`.
    Unlike in `SerialisedArtefact` and `Artefact`, filenames is never a glob but
    always the expanded list of files (if present).
    """
    user_serializer: str
    user_dtype: str
    filename: str | None = None
    filenames: list[str] | None = None

    def as_artefact(self) -> Artefact[Any]:
        filename = None
        filenames = None

        if self.filename is not None:
            filename = str((settings.EXECUTOR.user_data_dir / self.filename).resolve())
        elif self.filenames is not None:
            filenames = [str((settings.EXECUTOR.user_data_dir / fn).resolve()) for fn in self.filenames]
        else:
            raise ValueError('One of file or filenames has to be set!')

        return Artefact(
            serializer=self.user_serializer,
            dtype=self.user_dtype,
            filename=filename,
            filenames=filenames,
            fn_resolved=True
        )


def is_obj_user_artefact(obj: Any) -> bool:
    """
    Returns true, if the passed object has the correct signature to be a `UserArtefact`.
    """
    return type(obj) is dict and \
        'user_serializer' in obj and \
        'user_dtype' in obj and \
        ('filename' in obj or 'filenames' in obj)  # noqa: E127


def is_obj_serialized_artefact(obj: Any) -> bool:
    """
    Returns true, if the passed object has the correct signature to be a `SerializedArtefact`.

    isinstance(obj, SerializedArtefact))
    "The above use case is not supported." -> https://peps.python.org/pep-0589/
    """
    return type(obj) is dict and type(obj.get('serializer')) == str and type(obj.get('dtype')) == str


def is_obj_artefact_reference(obj: Any) -> bool:
    """
    Returns true, if the passed object has the correct signature to be a `ArtefactReference`.
    """
    return type(obj) is dict and type(obj.get('task_id')) == str and type(obj.get('artefact')) == str
