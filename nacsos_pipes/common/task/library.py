import os
import sys
import asyncio
import logging
import importlib
import inspect
from textwrap import dedent
from dataclasses import is_dataclass

from pathlib import Path
from types import GenericAlias, UnionType
from typing import Callable, Any, Mapping, is_typeddict, get_args, Sequence
from pydantic import ConfigDict, BaseModel

from nacsos_data.models.pipeline import KWARG, CPULoadClassification

from .. import settings
from .artefacts import Artefact

logger = logging.getLogger('nacsos-pipes.task.library')


class FunctionInfo(BaseModel):
    # module path of this function
    module: str
    # function name
    function: str
    # all arguments of this function (except context)
    # dict of key = param name, value = datatype or tuple(datatype, default value) or dict for artefact
    kwargs: dict[str, KWARG]
    # name to be displayed in the interface
    name: str
    # docstring of this function
    docstring: str
    # artefacts this function produces
    artefacts: Mapping[str, Artefact[Any]]
    # absolute path to file where this function is defined
    filepath: str
    # same, but stripped to the relative path (from configured root)
    filepath_rel: str
    # tags to be used in the interface for search
    tags: list[str] | None = None
    # a constant indicator on how computationally heavy the execution of this function would be
    est_cpu_load: CPULoadClassification = CPULoadClassification.MEDIUM
    # can be set to indicate a recommended time when to automatically
    # remove artefacts and other outputs (in days after execution)
    recommended_lifetime: int | None = None

    @property
    def full_name(self) -> str:
        return f'{self.module}.{self.function}'
    # TODO[pydantic]: The following keys were removed: `json_encoders`.
    # Check https://docs.pydantic.dev/dev-v2/migration/#changes-to-config for more information.
    model_config = ConfigDict(json_encoders={
        # Tell pydantic how to serialise an `Artefact`
        GenericAlias: lambda v: Artefact.try_generic_alias_serialisation(v)
    })


class LibraryFunctionInfo(FunctionInfo):
    # functions that can be used to estimate the runtime (sec) and memory (RAM) consumption
    # based on number of items to be processed
    memory_estimator: Callable[[int], int | float] | None = None
    runtime_estimator: Callable[[int], int | float] | None = None


def ensure_eventloop() -> asyncio.AbstractEventLoop:
    try:
        loop: asyncio.AbstractEventLoop = asyncio.get_running_loop()
    except RuntimeError:
        loop = asyncio.new_event_loop()
    finally:
        asyncio.set_event_loop(loop)
        return loop


class LibraryCache:
    def __init__(self, base_path: Path):
        self.base_path = base_path.resolve()
        self.package = str(self.base_path.resolve().parent)

        self.functions: list[LibraryFunctionInfo] = []

        # Library status to ensure there's no access during an unclear state
        self._ready: asyncio.Future[Sequence[FunctionInfo]] = asyncio.Future()
        self._ready_sync: bool = False

        # lookup index for FunctionInfo.full_name
        self._ix_funcs: dict[str, int] | None = None

    def get_library_sync(self) -> list[LibraryFunctionInfo]:
        return self.functions if self._ready_sync else []

    async def get_library(self) -> list[LibraryFunctionInfo]:
        if self._ready is not None:
            await self._ready
        return self.functions

    def clear_cache(self) -> None:
        ensure_eventloop()
        self._ready = asyncio.Future()
        self._ready_sync = False
        self.functions = []
        self._ix_funcs = None

    def reload(self) -> None:
        logger.info('Reloading library...')
        self.clear_cache()
        logger.debug(f'Going to iteratively import modules in {self.base_path}')
        for root, dirs, files in os.walk(self.base_path):
            for file in files:
                path = Path(root) / file
                if path.suffix == '.py' and not file.startswith('_'):
                    module_name = '.'.join(
                        [self.base_path.name, *path.relative_to(self.base_path).parts[:-1], path.stem]
                    )
                    try:
                        if module_name in sys.modules:
                            logger.debug(f'Reloading already imported module `{module_name}`')
                            importlib.reload(sys.modules[module_name])
                        else:
                            logger.debug(f'Loading `{module_name}` from from {path}')
                            importlib.import_module(module_name, package=self.package)
                    except Exception as e:
                        logger.warning(f'Failed to import {module_name}')
                        logger.exception(e)
        logger.debug('Done reloading library!')
        self._ready.set_result(self.functions)
        self._ready_sync = True

    @property
    async def ix_funcs(self) -> dict[str, int]:
        if self._ix_funcs is None:
            await self._ready
            self._ix_funcs = self._build_index()
        return self._ix_funcs

    @property
    def ix_funcs_sync(self) -> dict[str, int]:
        if not self._ready_sync:
            return {}
        if self._ix_funcs is None:
            self._ix_funcs = self._build_index()
        return self._ix_funcs

    def _build_index(self) -> dict[str, int]:
        return {func.full_name: i for i, func in enumerate(self.functions)}

    async def find_by_name(self, full_name: str) -> LibraryFunctionInfo | None:
        ix_funcs = await self.ix_funcs
        if full_name in ix_funcs:
            return self.functions[ix_funcs[full_name]]
        return None

    def find_by_name_sync(self, full_name: str) -> LibraryFunctionInfo | None:
        ix_funcs = self.ix_funcs_sync
        if full_name in ix_funcs:
            return self.functions[ix_funcs[full_name]]
        return None

    def func(self,
             name: str,
             artefacts: dict[str, Artefact[Any]],
             tags: list[str] | None = None,
             memory_estimator: Callable[[int], int | float] | None = None,
             runtime_estimator: Callable[[int], int | float] | None = None,
             est_cpu_load: CPULoadClassification = CPULoadClassification.MEDIUM,
             recommended_lifetime: int | None = None) -> Callable[[Any], Any]:

        def decorator(function):  # type:ignore
            from ...executor import TaskContext  # import down here to prevent circular import
            func_kwargs = {}
            try:
                # iterate over arguments and doing a best effort to transform to string
                for param in inspect.signature(function).parameters.values():
                    if param.annotation == TaskContext:
                        continue

                    func_kwargs[param.name] = param2dtype(param)

                func_info = LibraryFunctionInfo(
                    module=function.__module__,
                    filepath=function.__code__.co_filename,
                    filepath_rel=function.__code__.co_filename[len(str(self.base_path)) + 1:],
                    function=function.__code__.co_name,
                    kwargs=func_kwargs,
                    name=name,
                    docstring=dedent(function.__doc__),
                    artefacts=artefacts,
                    tags=tags,
                    memory_estimator=memory_estimator,
                    runtime_estimator=runtime_estimator,
                    recommended_lifetime=recommended_lifetime,
                    est_cpu_load=est_cpu_load,
                )

                ix_funcs = self._build_index()
                # if this function was already imported, override
                if func_info.full_name in ix_funcs:
                    logger.debug(f'Overriding `FunctionInfo` in library for `{func_info.full_name}`')
                    self.functions[ix_funcs[func_info.full_name]] = func_info
                else:
                    logger.debug(f'Adding `FunctionInfo` to library for `{func_info.full_name}`')
                    self.functions.append(func_info)
            except Exception as e:
                logger.error(f'Failed to add function {function} to library')
                logger.exception(e)

            def wrapper(*args: Any, **kwargs: Any) -> Callable[[Any], Any]:
                result = function(*args, **kwargs)
                return result

            return wrapper

        return decorator


def param2dtype(param: inspect.Parameter) -> KWARG:
    if is_dataclass(param.annotation):
        return KWARG(
            dtype=[param.annotation.__name__],
            params={
                param.name: param2dtype(param)
                for param in inspect.signature(param.annotation).parameters.values()
            }
        )
    if is_typeddict(param.annotation):
        return KWARG(
            dtype=[param.annotation.__name__],
            params={
                key: KWARG(dtype=[value.__name__])
                for key, value in param.annotation.__annotations__.items()
            }
        )

    optional = True if param.default is None or param.default != param.empty else None
    default = param.default if param.default != param.empty else None

    if hasattr(param.annotation, '__origin__') and param.annotation.__origin__ == Artefact:
        return KWARG(dtype=['Artefact'], optional=optional, default=default,
                     artefact=Artefact.type2artefact(param.annotation).as_dict())

    if hasattr(param.annotation, '__name__') and param.annotation.__name__ == 'Literal':
        return KWARG(dtype=['Literal'], optional=optional, default=default,
                     options=list(get_args(param.annotation)))

    if isinstance(param.annotation, UnionType):
        return KWARG(dtype=[arg.__name__ for arg in get_args(param.annotation) if arg.__name__ != 'NoneType'],
                     default=default, optional=optional)

    if isinstance(param.annotation, type) and isinstance(param.annotation, GenericAlias):
        # FIXME: also test for typing.List and translate to dtype="list" (which doesn't end up here)
        return KWARG(dtype=[param.annotation.__name__],
                     optional=optional, default=default,
                     generics=[arg.__name__ for arg in param.annotation.__args__])

    if isinstance(param.annotation, type):
        return KWARG(dtype=[param.annotation.__name__], default=default, optional=optional)

    return KWARG(dtype=[str(param.annotation)], default=default, optional=optional)


library = LibraryCache(base_path=Path(settings.LIBRARY.LIBRARY_PATH))

__all__ = ['library', 'FunctionInfo', 'LibraryFunctionInfo', 'CPULoadClassification']
