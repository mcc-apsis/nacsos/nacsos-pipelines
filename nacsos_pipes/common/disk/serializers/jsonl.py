import json
from pathlib import Path
from types import TracebackType
from dataclasses import asdict, is_dataclass
from typing import Generic, Literal, IO, Generator, get_args, TypeVar, Any, Protocol

from . import BaseSerializer


class DictLike(Protocol):
    __dataclass_fields__: dict[str, Any]

    def as_dict(self) -> dict[str, Any]:
        ...

    def dict(self) -> dict[str, Any]:
        ...


# DictLike = SupportsAsDict | SupportsDict | DataclassLike | BaseModel | dict[str, Any]
# T = TypeVar('T', SupportsAsDict , SupportsDict , DataclassLike , BaseModel , dict[str, Any])
T = TypeVar('T')


class JSONLSerializer(BaseSerializer[DictLike], Generic[T]):

    def __init__(self, workdir: Path | str | None = None, filename: str | None = None, filepath: str | None = None,
                 mode: Literal['r', 'w', 'a', 'x'] = 'r'):

        if filepath is None and workdir is not None and filename is not None:
            self.workdir: Path = workdir if type(workdir) is Path else Path(workdir)
            self.filename = filename
            self.filepath = (self.workdir / filename).resolve()
        elif filepath is not None:
            self.filepath = Path(filepath)
        else:
            raise AttributeError('No valid path given.')

        self.mode = mode
        self._file_pointer: IO[str] | None = None

    def __enter__(self) -> 'JSONLSerializer[T]':
        self.filepath.parent.mkdir(parents=True, exist_ok=True)
        self._file_pointer = open(self.filepath, mode=self.mode)
        return self

    def __exit__(self,
                 exctype: type[BaseException] | None,
                 excinst: BaseException | None,
                 exctb: TracebackType | None) -> None:
        if self._file_pointer is not None:
            self._file_pointer.close()

    def write(self, data: DictLike) -> None:
        self.write_line(data)

    def write_line(self, data: DictLike) -> None:
        if self._file_pointer is None:
            raise RuntimeError('No open file pointer!')
        if type(data) is dict:
            data_dict: dict[str, Any] = data
        elif hasattr(data, 'dict'):
            data_dict = data.dict()
        elif hasattr(data, 'as_dict'):
            data_dict = data.as_dict()
        elif hasattr(data, 'model_dump'):
            data_dict = data.model_dump()
        elif is_dataclass(data):
            data_dict = asdict(data)  # type: ignore[arg-type]  # FIXME
        else:
            raise ValueError('Expected some sort of dict!')
        data_str = json.dumps(data_dict)
        self._file_pointer.write(data_str + '\n')

    def write_lines(self, data: list[DictLike]) -> None:
        for data_i in data:
            self.write_line(data_i)

    def _deserialize_line(self, line: str) -> DictLike:
        dtype = get_args(self.__orig_class__)[0]  # type: ignore[attr-defined]
        data = json.loads(line)
        if dtype == dict:
            return data
        elif hasattr(dtype, 'from_dict'):
            return dtype.mo(data)
        elif hasattr(dtype, 'parse_obj'):
            return dtype.parse_obj(data)
        elif hasattr(dtype, 'model_validate'):
            return dtype.model_validate(data)
        else:
            raise ValueError('Not sure how to properly represent this object.', data)

    def read_lines(self) -> Generator[DictLike, None, None]:
        if self._file_pointer is None:
            raise RuntimeError('No open file pointer!')

        for line in self._file_pointer:
            yield self._deserialize_line(line)

    def read(self) -> Generator[DictLike, None, None]:
        yield from self.read_lines()
