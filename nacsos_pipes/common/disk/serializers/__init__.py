import json
from pathlib import Path
from abc import ABC, abstractmethod
from typing import TypeVar, Generic, get_args, Iterable
from pydantic.dataclasses import dataclass

DATA_TYPE = TypeVar("DATA_TYPE")


@dataclass
class BaseSerializer(ABC, Generic[DATA_TYPE]):
    filepath: Path

    def as_dict(self) -> dict[str, str | Path | None]:
        return {
            'serializer': self.__class__.__name__,
            'dtype': self.dtype.__name__ if self.dtype else None,
            'filepath': self.filepath,
        }

    def __str__(self) -> str:
        return json.dumps(self.as_dict())

    def __repr__(self) -> str:
        return json.dumps(self.as_dict())

    @property
    def dtype(self) -> type | None:
        if hasattr(self, '__orig_class__'):
            obj = self.__orig_class__
        elif hasattr(self, '__original__'):
            obj = self.__original__
        else:
            obj = self
        if get_args(obj):
            return get_args(obj)[0]
        return None

    @abstractmethod
    def write(self, data: DATA_TYPE) -> None:
        raise NotImplementedError

    @abstractmethod
    def read(self) -> DATA_TYPE | Iterable[DATA_TYPE]:
        raise NotImplementedError

    # TODO: read_batches
    # TODO: read_generator
    # TODO: write_batches
    # TODO: write line


from .jsonl import JSONLSerializer
from .wos import WebOfScienceSerializer
from .scopus import ScopusCSVSerializer

__all__ = ['BaseSerializer', 'JSONLSerializer', 'WebOfScienceSerializer', 'ScopusCSVSerializer']
