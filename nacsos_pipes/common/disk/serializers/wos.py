from pathlib import Path
from typing import Generator, TypeVar, Generic
from nacsos_data.util.academic.wos import read_wos_file
from nacsos_data.models.items.academic import AcademicItemModel

from . import BaseSerializer

T = TypeVar('T')


class WebOfScienceSerializer(BaseSerializer[AcademicItemModel], Generic[T]):
    def __init__(self, workdir: Path | str | None = None, filename: str | None = None, filepath: str | None = None):
        if filepath is None and workdir is not None and filename is not None:
            self.workdir: Path = workdir if type(workdir) is Path else Path(workdir)
            self.filename = filename
            self.filepath = (self.workdir / filename).resolve()
        elif filepath is not None:
            self.filepath = Path(filepath)
        else:
            raise AttributeError('No valid path given.')

    def write(self, data: AcademicItemModel) -> None:
        raise NotImplementedError('Writing WebOfScience files is not supported (yet).')

    def read(self) -> Generator[AcademicItemModel, None, None]:
        yield from read_wos_file(filepath=str(self.filepath))
