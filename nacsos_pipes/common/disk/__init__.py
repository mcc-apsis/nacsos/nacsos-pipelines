from pathlib import Path


def xpath(path: Path | str, create_dir: bool = False) -> Path:
    """
    Returns the given path (relative to this script),
    and its parent directories actually exist then.

    Args:
        path: The path to check.
        create_dir: Interpret the path as a directory,
            and create it if it doesn't exist.
    """
    path_ = (Path(__file__).parent / path).resolve()
    if create_dir:
        path_.mkdir(parents=True, exist_ok=True)
    else:
        path_.parent.mkdir(parents=True, exist_ok=True)
    return path_
