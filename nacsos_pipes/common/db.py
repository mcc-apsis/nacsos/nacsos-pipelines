from nacsos_data.db import DatabaseEngine
from nacsos_data.db import DatabaseEngineAsync

from .config import settings


def get_db_engine() -> DatabaseEngine:
    return DatabaseEngine(host=settings.DB_CORE.HOST, port=settings.DB_CORE.PORT,
                          user=settings.DB_CORE.USER, password=settings.DB_CORE.PASSWORD,
                          database=settings.DB_CORE.DATABASE, debug=settings.DB_CORE.DEBUG)


def get_db_engine_async() -> DatabaseEngineAsync:
    return DatabaseEngineAsync(host=settings.DB_CORE.HOST, port=settings.DB_CORE.PORT,
                               user=settings.DB_CORE.USER, password=settings.DB_CORE.PASSWORD,
                               database=settings.DB_CORE.DATABASE, debug=settings.DB_CORE.DEBUG)


db_engine = get_db_engine()
db_engine_async = get_db_engine_async()
