from enum import Enum
from pymitter import EventEmitter


class Event(str, Enum):
    # A task in our database changed its status (e.g. is now running)
    # They all carry `task_id: str` and `status: TaskStatus`.
    TaskChangedStatus = 'TaskChangedStatus_*'
    TaskChangedStatus_pending = 'TaskChangedStatus_pending'
    TaskChangedStatus_running = 'TaskChangedStatus_running'
    TaskChangedStatus_completed = 'TaskChangedStatus_completed'
    TaskChangedStatus_failed = 'TaskChangedStatus_failed'
    TaskChangedStatus_cancelled = 'TaskChangedStatus_cancelled'
    # A new task was submitted to the queue (should carry `task_id: str`)
    NewTaskReceived = 'NewTaskReceived'
    # Triggered when a task is removed
    TaskDeleted = 'TaskDeleted'


eventbus = EventEmitter(delimiter='_', wildcard=True)

__all__ = ['eventbus', 'Event']
