import logging
from fastapi import Depends, HTTPException, status as http_status, Header
from fastapi.security import OAuth2PasswordBearer
from nacsos_data.db.crud.pipeline import read_task_by_id

from nacsos_data.models.pipeline import TaskModel
from nacsos_data.models.users import UserModel, UserInDBModel
from nacsos_data.models.projects import ProjectPermission
from nacsos_data.util.auth import Authentication, InsufficientPermissionError, InvalidCredentialsError, UserPermissions

from ..common.db import db_engine, db_engine_async
from ..common.config import settings

logger = logging.getLogger('nacsos-pipes.server.security')


class UserTaskProjectPermissions(UserPermissions):
    task: TaskModel


class InsufficientPermissions(Exception):
    status = http_status.HTTP_403_FORBIDDEN


auth_helper = Authentication(engine=db_engine_async,
                             token_lifetime_minutes=settings.API.ACCESS_TOKEN_EXPIRE_MINUTES,
                             default_user=None)
oauth2_scheme = OAuth2PasswordBearer(tokenUrl=settings.CORE_API_URL + '/login/token', auto_error=False)


async def get_current_user(token: str = Depends(oauth2_scheme)) -> UserInDBModel:
    try:
        return await auth_helper.get_current_user(token_id=token)
    except (InvalidCredentialsError, InsufficientPermissionError) as e:
        raise HTTPException(
            status_code=http_status.HTTP_401_UNAUTHORIZED,
            detail=repr(e),
            headers={'WWW-Authenticate': 'Bearer'},
        )


async def get_current_active_user(current_user: UserModel = Depends(get_current_user)) -> UserModel:
    if not current_user.is_active:
        raise HTTPException(status_code=http_status.HTTP_400_BAD_REQUEST, detail='Inactive user')
    return current_user


def get_current_active_superuser(current_user: UserModel = Depends(get_current_active_user)) -> UserModel:
    if not current_user.is_superuser:
        raise HTTPException(
            status_code=http_status.HTTP_400_BAD_REQUEST, detail="The user doesn't have enough privileges"
        )
    return current_user


class UserPermissionChecker:
    def __init__(self,
                 permissions: list[ProjectPermission] | ProjectPermission | None = None,
                 fulfill_all: bool = True):
        self.permissions = permissions
        self.fulfill_all = fulfill_all

        # convert singular permission to list for unified processing later
        if type(self.permissions) is str:
            self.permissions = [self.permissions]

    async def __call__(self,
                       x_project_id: str = Header(),
                       current_user: UserModel = Depends(get_current_active_user)) -> UserPermissions:
        """
        This function checks the whether a set of required permissions is fulfilled
        for the given project for the currently active user.
        The list of `permissions` corresponds to boolean fields in the respective `ProjectPermissions` instance.
        If left empty, only the existence of such an instance is checked – meaning whether or not the user
        is allowed to see or access the project in one way or another.

        If at least one permission is not fulfilled or no instance exists, this function raises a 403 HTTPException

        :return: `ProjectPermissions` if permissions are fulfilled, exception otherwise
        :raises HTTPException if permissions are not fulfilled
        """
        try:
            return await auth_helper.check_permissions(project_id=x_project_id,
                                                       user=current_user,
                                                       required_permissions=self.permissions,
                                                       fulfill_all=self.fulfill_all)

        except (InvalidCredentialsError, InsufficientPermissionError) as e:
            raise InsufficientPermissions(repr(e))


class UserTaskPermissionChecker:
    def __init__(self,
                 permissions: list[ProjectPermission] | ProjectPermission | None = None,
                 fulfill_all: bool = True):
        self.permissions = permissions
        self.fulfill_all = fulfill_all

        # convert singular permission to list for unified processing later
        if type(self.permissions) is str:
            self.permissions = [self.permissions]

    async def __call__(self,
                       x_task_id: str = Header(),
                       x_project_id: str = Header(),
                       current_user: UserModel = Depends(get_current_active_user)) -> UserTaskProjectPermissions:
        try:
            permissions = await auth_helper.check_permissions(project_id=x_project_id,
                                                              user=current_user,
                                                              required_permissions=self.permissions,
                                                              fulfill_all=self.fulfill_all)

            task = read_task_by_id(task_id=x_task_id, engine=db_engine)

            if task is None or str(task.project_id) != str(permissions.permissions.project_id):
                # TODO: do we also want to check if the user_id overlaps?
                raise InsufficientPermissionError('Invalid task or project permissions.')

            return UserTaskProjectPermissions(user=permissions.user,
                                              permissions=permissions.permissions,
                                              task=task)

        except (InvalidCredentialsError, InsufficientPermissionError) as e:
            raise InsufficientPermissions(repr(e))


__all__ = ['UserPermissionChecker', 'UserPermissions',
           'UserTaskProjectPermissions', 'UserTaskPermissionChecker',
           'InsufficientPermissionError', 'InvalidCredentialsError', 'InsufficientPermissions',
           'auth_helper', 'oauth2_scheme',
           'get_current_user', 'get_current_active_user', 'get_current_active_superuser']
