import re
import unicodedata
from shutil import rmtree
from typing import AsyncGenerator
from uuid import uuid4
from pathlib import Path
from typing_extensions import TypedDict

import aiofiles
from fastapi import APIRouter, UploadFile, Depends
from fastapi.responses import FileResponse
from nacsos_data.util.auth import UserPermissions
from pydantic import BaseModel
from tempfile import TemporaryDirectory

from ...security import UserTaskPermissionChecker, UserTaskProjectPermissions, UserPermissionChecker
from ....common import get_logger, settings
from ....common.disk.outputs import get_outputs_flat, get_log, delete_files, delete_task_directory, zip_folder

logger = get_logger('nacsos-pipes.api.route.artefacts')
router = APIRouter()

logger.info('Setting up artefacts route')


class FileOnDisk(TypedDict):
    path: str
    size: int


@router.get('/list', response_model=list[FileOnDisk])
def get_artefacts(permissions: UserTaskProjectPermissions = Depends(UserTaskPermissionChecker('artefacts_read'))) \
        -> list[FileOnDisk]:
    task_id = permissions.task.task_id  # FIXME assert`task_id is not None`
    return [
        FileOnDisk(path=file[0],
                   size=file[1])  # type: ignore[typeddict-item] # FIXME
        for file in get_outputs_flat(task_id=str(task_id), include_fsize=True)
    ]


@router.get('/log', response_model=str)
def get_task_log(permissions: UserTaskProjectPermissions = Depends(UserTaskPermissionChecker('artefacts_read'))) \
        -> str | None:
    task_id = permissions.task.task_id  # FIXME assert`task_id is not None`

    # TODO stream the log instead of sending the full file
    #  via: https://fastapi.tiangolo.com/advanced/custom-response/#streamingresponse
    #  or: https://fastapi.tiangolo.com/advanced/websockets/
    # probably best to return tail by default
    return get_log(task_id=str(task_id))


@router.get('/file', response_class=FileResponse)
def get_file(filename: str,
             permissions: UserTaskProjectPermissions = Depends(UserTaskPermissionChecker('artefacts_read'))) -> FileResponse:
    return FileResponse(settings.EXECUTOR.target_dir / filename)


async def tmp_path() -> AsyncGenerator[Path, None]:
    td = TemporaryDirectory()
    tpath = Path(td.name)
    tpath.mkdir(exist_ok=True, parents=True)
    try:
        yield tpath
    finally:
        rmtree(tpath)


@router.get('/files', response_class=FileResponse)
def get_archive(permissions: UserTaskProjectPermissions = Depends(UserTaskPermissionChecker('artefacts_read')),
                tmp_dir: Path = Depends(tmp_path)) -> FileResponse:
    task_id = permissions.task.task_id  # FIXME assert`task_id is not None`
    zip_folder(task_id=str(task_id), target_file=str(tmp_dir / 'archive.zip'))
    return FileResponse(str(tmp_dir / 'archive.zip'))


@router.post('/files/upload', response_model=str)
async def upload_file(file: UploadFile,
                      folder: str | None = None,
                      permissions: UserPermissions = Depends(UserPermissionChecker('artefacts_edit'))) \
        -> str:
    if folder is None:
        folder = str(uuid4())

    # make this a safe and clean filename by removing all non-ascii characters and only
    # allowing `a-z`/`A-Z` and `0-9` and `_` and `-` and `.`; all whitespaces will be replaced by `-`.
    filename_ = str(file.filename)
    filename_ = unicodedata.normalize('NFKD', filename_).encode('ascii', 'ignore').decode('ascii')
    filename_ = re.sub(r'[^a-zA-Z0-9_\-.\r\n\t\f\v ]', '', filename_)
    filename_ = re.sub(r'[\r\n\t\f\v ]+', '-', filename_).strip('-_')

    filename = settings.EXECUTOR.user_data_dir / folder / filename_
    filename.parent.mkdir(parents=True, exist_ok=True)
    async with aiofiles.open(filename, 'wb') as f:
        while content := await file.read(1048576):  # async read chunk
            await f.write(content)

    return str(Path(folder) / filename_)


@router.post('/files/upload-many', response_model=list[str])
async def upload_files(file: list[UploadFile],
                       folder: str | None = None,
                       permissions: UserPermissions = Depends(UserPermissionChecker('artefacts_edit'))) \
        -> list[str]:
    if folder is None:
        folder = str(uuid4())
    return [await upload_file(file=f, folder=folder) for f in file]


class DeletionRequest(BaseModel):
    task_id: str
    files: list[str]


@router.delete('/files')
def delete_files_(req: DeletionRequest,
                  permissions: UserTaskProjectPermissions = Depends(UserTaskPermissionChecker('artefacts_edit'))) \
        -> None:
    if str(permissions.task.task_id) == str(req.task_id):
        # TODO do this for user_data_files as well
        delete_files(task_id=req.task_id, files=req.files)


@router.delete('/task')
def delete_task_files(permissions: UserTaskProjectPermissions = Depends(UserTaskPermissionChecker('artefacts_edit'))) \
        -> None:
    task_id = permissions.task.task_id  # FIXME assert`task_id is not None`
    delete_task_directory(task_id=str(task_id))
