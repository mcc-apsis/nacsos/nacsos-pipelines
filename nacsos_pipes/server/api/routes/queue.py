import json

from nacsos_data.util.auth import UserPermissions
from pydantic import StringConstraints
from fastapi import APIRouter, Query, Depends, status as http_status, HTTPException

from nacsos_data.db.crud.pipeline import query_tasks, read_task_by_id
from nacsos_data.models.users import UserModel
from nacsos_data.models.pipeline import SubmittedTask, TaskStatus, TaskModel

from ....common import get_logger, db_engine
from ....common.errors import UnknownTaskID, TaskSubmissionFailed
from ....common.queue import task_queue
from ...security import get_current_active_superuser, UserPermissionChecker
from typing_extensions import Annotated

logger = get_logger('nacsos-pipes.api.route.queue')
router = APIRouter()

logger.info('Setting up queue route')


@router.get('/list', response_model=list[TaskModel])
async def get_all(superuser: UserModel = Depends(get_current_active_superuser)) -> list[TaskModel]:
    tasks = query_tasks(engine=db_engine)
    if tasks is None:
        return []
    return tasks


@router.get('/list/{status}', response_model=list[TaskModel])
async def get_by_status(status: TaskStatus,
                        superuser: UserModel = Depends(get_current_active_superuser)) -> list[TaskModel]:
    tasks = query_tasks(engine=db_engine, status=status)
    if tasks is None:
        return []
    return tasks


@router.get('/project/list', response_model=list[TaskModel])
async def get_all_for_project(permissions: UserPermissions = Depends(UserPermissionChecker('pipelines_read'))) \
        -> list[TaskModel]:
    tasks = query_tasks(engine=db_engine, project_id=permissions.permissions.project_id)
    if tasks is None:
        return []
    return tasks


@router.get('/project/list/{status}', response_model=list[TaskModel])
async def get_by_status_for_project(status: TaskStatus,
                                    permissions: UserPermissions = Depends(UserPermissionChecker('pipelines_read'))) \
        -> list[TaskModel]:
    tasks = query_tasks(engine=db_engine, status=status, project_id=permissions.permissions.project_id)
    if tasks is None:
        return []
    return tasks


OrderBy = Annotated[str, StringConstraints(pattern=r'^[A-Za-z\-_]+,(asc|desc)$')]


@router.get('/search', response_model=list[TaskModel])
async def search_tasks(function_name: str | None = None,
                       fingerprint: str | None = None,
                       user_id: str | None = None,
                       location: str | None = None,
                       status: str | None = None,
                       order_by_fields: list[OrderBy] | None = Query(None),
                       permissions: UserPermissions = Depends(UserPermissionChecker('pipelines_read'))
                       ) -> list[TaskModel]:
    order_by_fields_parsed = None
    if order_by_fields is not None:
        order_by_fields_parsed = [
            (parts[0], parts[1] == 'asc')
            for field in order_by_fields
            if (parts := field.split(',')) is None
        ]

    params = {}
    if function_name is not None:
        params['function_name'] = function_name
    if fingerprint is not None:
        params['fingerprint'] = fingerprint
    if user_id is not None:
        params['user_id'] = user_id
    if location is not None:
        params['location'] = location
    if status is not None:
        params['status'] = status

    tasks = query_tasks(engine=db_engine,
                        project_id=permissions.permissions.project_id,
                        order_by_fields=order_by_fields_parsed,
                        **params)

    if tasks is None:
        return []
    return tasks


@router.get('/task/{task_id}', response_model=TaskModel)
async def get_task(task_id: str,
                   permissions: UserPermissions = Depends(UserPermissionChecker('pipelines_edit'))) -> TaskModel:
    task = read_task_by_id(task_id=task_id, engine=db_engine)

    if task is None or str(task.project_id) != str(permissions.permissions.project_id):
        # TODO: do we also want to check if the user_id overlaps?
        raise HTTPException(status_code=http_status.HTTP_400_BAD_REQUEST, detail='Nope.')

    if task is None:
        raise UnknownTaskID(f'Task does not exist with ID {task_id}')
    return task


@router.get('/status/{task_id}', response_model=TaskStatus)
async def get_status(task_id: str,
                     permissions: UserPermissions = Depends(UserPermissionChecker('pipelines_read'))) \
        -> TaskStatus | str:
    task = read_task_by_id(task_id=task_id, engine=db_engine)

    if task is None or str(task.project_id) != str(permissions.permissions.project_id):
        # TODO: do we also want to check if the user_id overlaps?
        raise HTTPException(status_code=http_status.HTTP_400_BAD_REQUEST, detail='Nope.')

    if task.status is None:
        raise UnknownTaskID(f'Task does not exist with ID {task_id}')
    return task.status


@router.post('/force-run/{task_id}')
async def force_run(task_id: str,
                    permissions: UserPermissions = Depends(UserPermissionChecker('pipelines_edit'))) -> None:
    task = read_task_by_id(task_id=task_id, engine=db_engine)

    if task is None or str(task.project_id) != str(permissions.permissions.project_id):
        # TODO: do we also want to check if the user_id overlaps?
        raise HTTPException(status_code=http_status.HTTP_400_BAD_REQUEST, detail='Nope.')

    await task_queue.force_execute(task_id=task_id)


@router.put('/submit/tasks', response_model=list[str | None])
async def submit_bulk(tasks: list[SubmittedTask],
                      permissions: UserPermissions = Depends(UserPermissionChecker('pipelines_edit'))) \
        -> list[str | None]:
    if not all([str(task.project_id) == str(permissions.permissions.project_id) for task in tasks]):
        # TODO: do we also want to check if the user_id overlaps?
        raise HTTPException(status_code=http_status.HTTP_400_BAD_REQUEST, detail='Nope.')

    # FIXME: We shouldn't re-run everything if only one task is marked as force re-run.
    #        Go through the dependencies first and only mark respective downstream tasks to be re-run!
    check_fingerprints = all([not task.force_run for task in tasks])
    logger.debug(f'Received tasks to add. Force run settings: {[task.force_run for task in tasks]} '
                 f'-> check_fingerprints = {check_fingerprints}')

    for task in tasks:
        if type(task.params) is str:
            task.params = json.loads(task.params)

    return await task_queue.add_tasks(tasks=tasks, check_fingerprints=check_fingerprints)


@router.put('/submit/task', response_model=str)
async def submit_single(task: SubmittedTask,
                        permissions: UserPermissions = Depends(UserPermissionChecker('pipelines_edit'))) -> str:
    if not str(task.project_id) == str(permissions.permissions.project_id):
        # TODO: do we also want to check if the user_id overlaps?
        raise HTTPException(status_code=http_status.HTTP_400_BAD_REQUEST, detail='Nope.')

    if type(task.params) is str:
        task.params = json.loads(task.params)
    task_id = await task_queue.add_task(
        task=task,
        check_fingerprint=not task.force_run
    )
    if task_id is not None:
        return task_id
    raise TaskSubmissionFailed('Did not successfully submit the task to the queue.')


@router.put('/cancel/{task_id}')
async def cancel_task(task_id: str,
                      permissions: UserPermissions = Depends(UserPermissionChecker('pipelines_edit'))) -> None:
    task = read_task_by_id(task_id=task_id, engine=db_engine)

    if task is None or str(task.project_id) != str(permissions.permissions.project_id):
        # TODO: do we also want to check if the user_id overlaps?
        raise HTTPException(status_code=http_status.HTTP_400_BAD_REQUEST, detail='Nope.')
    task_queue.cancel_task(task_id=task_id)


@router.delete('/task/{task_id}')
async def delete_task(task_id: str,
                      permissions: UserPermissions = Depends(UserPermissionChecker('pipelines_edit'))) -> None:
    task = read_task_by_id(task_id=task_id, engine=db_engine)

    if task is None or str(task.project_id) != str(permissions.permissions.project_id):
        # TODO: do we also want to check if the user_id overlaps?
        raise HTTPException(status_code=http_status.HTTP_400_BAD_REQUEST, detail='Nope.')
    task_queue.remove_task(task_id=task_id)
