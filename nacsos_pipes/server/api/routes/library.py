from fastapi import APIRouter, Query

from ....common import get_logger, library
from ....common.errors import LibraryFunctionNotFoundError
from ....common.task.library import FunctionInfo, LibraryFunctionInfo

logger = get_logger('nacsos-pipes.api.route.library')
router = APIRouter()

logger.info('Setting up library route')


@router.get('/list', response_model=list[FunctionInfo], response_model_exclude_none=True)
async def get_full_library() -> list[LibraryFunctionInfo]:
    return await library.get_library()


@router.get('/info/{func_name}', response_model=FunctionInfo)
async def get_function_info(func_name: str) -> LibraryFunctionInfo:
    result = await library.find_by_name(full_name=func_name)
    if result is not None:
        return result
    raise LibraryFunctionNotFoundError(f'Function not found for the name `{func_name}`')


@router.get('/infos', response_model=list[FunctionInfo])
async def get_function_infos(func_name: list[str] = Query()) -> list[LibraryFunctionInfo]:
    func_infos = [await library.find_by_name(full_name=func_name_) for func_name_ in func_name]
    return [func_info for func_info in func_infos if func_info is not None]


@router.patch('/refresh', response_model=int)
async def reload_library() -> int:
    library.reload()
    return len(await library.get_library())
