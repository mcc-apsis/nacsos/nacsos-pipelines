from fastapi import APIRouter
from .routes import library
from .routes import queue
from .routes import artefacts
from .routes import ping

# this router proxies all /api endpoints
router = APIRouter()

router.include_router(library.router, prefix='/library', tags=['library'])
router.include_router(queue.router, prefix='/queue', tags=['queue'])
router.include_router(artefacts.router, prefix='/artefacts', tags=['artefacts'])
router.include_router(ping.router, prefix='/ping', tags=['ping'])
