import mimetypes

from fastapi import FastAPI
from fastapi.middleware.trustedhost import TrustedHostMiddleware
from fastapi.middleware.gzip import GZipMiddleware
from fastapi.middleware.cors import CORSMiddleware

from .api import router as api_router
from .middlewares import ErrorHandlingMiddleware
from ..common import settings, get_logger, db_engine

mimetypes.init()

logger = get_logger('nacsos-pipes.server')

app = FastAPI(openapi_url=settings.API.OPENAPI_FILE,
              openapi_prefix=settings.API.OPENAPI_PREFIX,
              root_path=settings.API.ROOT_PATH,
              separate_input_output_schemas=False)

logger.debug('Setting up server and middlewares')
mimetypes.add_type('application/javascript', '.js')

app.add_middleware(ErrorHandlingMiddleware)
if settings.API.HEADER_CORS:
    app.add_middleware(CORSMiddleware,
                       allow_origins=settings.API.CORS_ORIGINS,
                       allow_methods=['GET', 'POST', 'DELETE', 'POST', 'PUT', 'OPTIONS'],
                       allow_headers=['*'],
                       allow_credentials=True)
    logger.info(f'CORSMiddleware will accept the following origins: {settings.API.CORS_ORIGINS}')
app.add_middleware(GZipMiddleware, minimum_size=1000)

logger.debug('Setup routers')
app.include_router(api_router, prefix='/api')


# app.mount('/', StaticFiles(directory=settings.SERVER.STATIC_FILES, html=True), name='static')


@app.on_event("startup")
async def on_startup() -> None:
    db_engine.startup()
