flake8==7.0.0
tox==4.12.1
pytest==8.0.0
pytest-cov==4.1.0
mypy==1.8.0
types-aiofiles==23.2.0.20240106
types-toml==0.10.8.7
types-psutil==5.9.5.20240205